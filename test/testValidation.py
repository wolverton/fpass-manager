from fpassmgr.validation import *
from fpassmgr.settings import Settings
from fpassmgr.manager import *
import os
import shutil
import unittest


class UITester(unittest.TestCase):

    def setUp(self):
        Settings.debug = True
        Settings.work_dir = "."
        Settings.template_dir = "../files/"
        # Delete any directories that would be created during test
        for test in ['Br1Cu2-225', 'O2Ti1-136']:
            simplePath = get_problem_directory(test)
            if os.path.isdir(simplePath):
                shutil.rmtree(simplePath)

    def tearDown(self):
        self.setUp()

    def test_result_collector(self):
        # Create the example directory
        name = UI.prepare_solution('files/simple.mint')
        fpass_dir = get_solution_directory(name)

        # Copy in one example, verify energy the quorum has not been reached
        shutil.copytree('files/complete-run-example', os.path.join(fpass_dir, '0'))
        s, e = get_solution(name, 0)
        self.assertEqual(-8.81103633, e, "Wrong energy")
        opt, a, e = FPASSValidation.get_optimal_solution(name)
        self.assertEqual(opt, None, "Should not have returned a structure")

        # Copy in another example, see if solution grabber returns a structure (equal to first one)
        for i in range(1,10):
            shutil.copytree('files/complete-run-example', os.path.join(fpass_dir, str(i)))
        opt, a, e = FPASSValidation.get_optimal_solution(name)
        self.assertTrue(Mint.compare(opt, s))
        self.assertEqual(a, 10)

    def test_simple_validation(self):
        # Create a directory containing the file structure to be compiled
        if os.path.isdir('tempDir'):
            shutil.rmtree('tempDir')
        os.mkdir("tempDir")
        strc = "tempDir/strc.vasp"
        Mint.convert('files/fcc.mint','vasp', strc)

        # Check that validation has not been started
        goal = 'files/fcc.mint'
        self.assertEqual(Validation.validate_solution(strc, goal), "Not started")

        # Start the calculation
        Validation.start_validation(strc)
        self.assertEqual(Validation.validate_solution(strc, goal), "In progress")

        # Fake the calculations finishing (tests for calculations are in testQMPY.py)
        shutil.rmtree("tempDir/calc/relaxation")
        shutil.copytree("files/qmpy-test/relaxation", "tempDir/calc/relaxation")
        shutil.copytree("files/qmpy-test/static", "tempDir/calc/static")

        # Stability test
        result, reaction = Validation._assess_stability("tempDir/calc", True)
        self.assertTrue("[Check]" in result)

        target_volume, target_spg = Validation._get_goal_structure('files/fcc.mint')
        # Volume change test
        result = Validation._assess_volume_change("tempDir/calc", target_volume)
        self.assertTrue("acceptable range" in result)

        # Space group test
        result = Validation._assess_space_group("tempDir/calc", target_spg)
        self.assertTrue("[Check]" in result)

        # Test calling all of them
        result = Validation.validate_solution(strc, goal)
        self.assertNotEqual(result, "In progress")

        # Done here
        shutil.rmtree("tempDir")

    def test_fpass_validation(self):
        name = UI.prepare_solution('files/rutile.mint', 'files/rutile.xray')
        fpass_dir = get_solution_directory(name)

        ## Copy in answers
        for i in range(10):
            shutil.copytree('files/complete-run-example', os.path.join(fpass_dir, str(i)))

        ## Check that answer is correct
        best_result, agreement, energy = FPASSValidation.get_optimal_solution(name)
        self.assertTrue(Mint.compare(best_result, "files/rutile.known", tol=0.05))

        ## Start validation
        FPASSValidation.start_validation(name)
        self.assertTrue(os.path.isdir(FPASSValidation.get_validation_directory(name)))

        ## Check that result is listed as in progress
        status = FPASSValidation.validate_solution(name)
        self.assertEqual(status, "In progress")

        ## Check x-ray match result
        xray_result = FPASSValidation._assess_rFactor(name, best_result, read_last_result = True)
        self.assertTrue("Decent" in xray_result)
        xray_result = FPASSValidation._assess_rFactor(name, best_result, read_last_result = True)

        ## Check x-ray pareto result
        xray_result = FPASSValidation._assess_pareto(name)
        self.assertTrue("[Check]" in xray_result)

        ## Check prototype result
        prototype = FPASSValidation._assess_prototype_match(name, best_result)
        self.assertTrue("already known" in prototype)
        
if __name__ == '__main__':
    unittest.main()
