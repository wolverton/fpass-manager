from fpassmgr import *
from qmpy import *
import os
import tempfile
import unittest
import copy
import shutil

class UITester(unittest.TestCase):
    ''' Acceptable miss rate '''
    miss_rate = 0.0001 # One out of every 10000

    def get_structures(self, prototype, formula):
        filtered = Formation.objects.filter(fit = "standard", calculation__path__contains = '/' + prototype + '/')
        filtered = filtered.filter(composition__generic = formula)
        filtered = filtered.all().prefetch_related('calculation__input')
        filtered = filtered.all().prefetch_related('calculation__input__composition')
        return  [ x.calculation.input for x in filtered if not (x.calculation is None or x.calculation.input is None)]

    def get_fail_rate(self, library):
        biggest = -1
        total_count = 0
        for p, e in library.iteritems():
            total_count += len(e)
            if len(e) > biggest: biggest = len(e)
        nFail = total_count - biggest
        return float(nFail) / float(total_count), nFail

    def prototype(self, name, formula):
        print >>fp, "Started working on " + name
        fp.flush()
        strcs = self.get_structures(name, formula)
        print >>fp, "\tLoaded %d %s structures."%(len(strcs), name)
        fp.flush()
        library = PrototypeLibraryFactory.get_unique_prototypes(strcs)
        total_size = 0
        for e in library.values(): total_size += len(e)
        print >>fp, "\tMatched %d to %d prototypes"%(total_size, len(library))
        rate, number = self.get_fail_rate(library)
        print >>fp, "\tFail rate: %.3e (%d)"%(rate, number)
        fp.flush()
        PrototypeLibraryFactory.save_to_directory(library, os.path.join('results', name))
        self.assertTrue(rate < self.miss_rate)

    def test_a1(self):
        self.prototype('A1', 'A')

    def test_d019(self):
        self.prototype('D0_19', 'AB3')

    def test_b2(self):
        self.prototype('B2', 'AB')

    def test_a13(self):
        self.prototype('A13', 'A')

    def test_spinel(self):
        self.prototype('spinel', 'AB2C4')

    def test_a20(self):
        self.prototype('A20', 'A')

    def test_b19(self):
        self.prototype('B19', 'AB')

    def test_a12(self):
        self.prototype('A12', 'A')

    def test_garnet(self):
        self.prototype('garnet', 'A2B3C3D12')

    def test_ilmenite(self):
        self.prototype('ilmenite', 'ABC3')
    
if __name__ == '__main__':
    fp = open('test.log', 'w')
    if os.path.isdir('results'):
    shutil.rmtree('results')
    os.mkdir('results')
    PrototypeLibraryFactory.print_status = True
    unittest.main()
