from fpassmgr.mint import Mint
from fpassmgr.util import xray_data_is_processed
from qmpy import *
import os
import tempfile
import unittest

class UITester(unittest.TestCase):

    def test_get_spacegroup(self):
        self.assertEqual(Mint.get_space_group_number('P1'),1)
        self.assertTrue(Mint.get_space_group_number('asdf') is None)

    def test_compare(self):
        self.assertTrue(Mint.compare('files/rutile.known', 'files/rutile.known'))
        self.assertFalse(Mint.compare('files/simple.mint', 'files/rutile.known'))

    def test_convert(self):
        temp = tempfile.mkstemp()[1]
        out = Mint.convert('files/rutile.known', 'vasp', filename=temp)
        self.assertTrue(Mint.compare('files/rutile.known', temp))
        fp = open(temp, 'w')
        print >> fp, out
        fp.close()
        self.assertTrue(Mint.compare('files/rutile.known', temp))
        os.remove(temp)
    
    def test_space_group(self):
        self.assertEqual(Mint.get_space_group('files/fcc.mint'),225)
        x = Structure.objects.all()[0]
        target = x.spacegroup.number
        self.assertEqual(Mint.get_space_group(x, input_type='qmpy'), target)
        self.assertEqual(Mint.get_space_group(io.poscar.write(x), input_type='string'),target)

    def test_xray(self):
        rFactor = Mint.get_xray_match('files/rutile.known', 'files/rutile.xray')
        self.assertAlmostEqual(rFactor, 0.123, 2)
        rFactor = Mint.get_xray_match('files/rutile.known', 'files/rutile.xray', refine=True)
        self.assertAlmostEqual(rFactor, 0.115, 2)
        rFactor, pattern = Mint.get_xray_match('files/rutile.known', 'files/rutile.xray', refine=True, get_pattern=True)
        self.assertTrue(len(pattern) > 1)
        self.assertFalse(xray_data_is_processed('files/Y3NiAl3Ge2.xray'))
        Mint.get_xray_match('files/Y3NiAl3Ge2.known', 'files/Y3NiAl3Ge2.xray', refine=True, rietveld=True)


if __name__ == '__main__':
    unittest.main()
