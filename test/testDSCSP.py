from fpassmgr.settings import Settings
from fpassmgr.dscsp import DSCSP
from fpassmgr.user import UI
from fpassmgr import manager as mgr
import shutil
import os
import unittest

class UITester(unittest.TestCase):

    def setUp(self):
        Settings.debug = True
        Settings.work_dir = "temp"
        if os.path.isdir(Settings.work_dir):
            shutil.rmtree(Settings.work_dir)
        os.mkdir(Settings.work_dir)
        Settings.template_dir = "../files/"

    def tearDown(self):
        shutil.rmtree("temp")

    def test_get_applicable(self):
        name = UI.prepare_solution(os.path.join(os.path.dirname(__file__),'files','Y3NiAl3Ge2.mint'))
        prototypes = DSCSP._get_matching_prototypes(name)
        self.assertTrue(len(prototypes) > 0)
        prototypes = DSCSP._get_matching_prototypes(name)
        self.assertTrue(len(prototypes) > 0)

    def test_prototype_search(self):
        name = UI.prepare_solution('files/Y3NiAl3Ge2.mint')
        DSCSP.gather_matching_prototypes(name)
        p_dir = get_prototype_directory(name)
        self.assertTrue(os.path.isdir(p_dir))
        
if __name__ == '__main__':
    unittest.main()
