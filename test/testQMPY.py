from fpassmgr.settings import Settings
from fpassmgr.qmpy import *
import os
import tempfile
import unittest
import shutil

class UITester(unittest.TestCase):

    def setUp(self):
        Settings.debug = True
        Settings.template_dir = "../files/"
        for dir in ['tempDir']:
            if os.path.isdir(dir):
                shutil.rmtree(dir)
        os.mkdir('tempDir')
    
    #@unittest.skipUnless(os.path.isfile(Settings.qstat), 'requires PBS')
    def test_energy_calc(self):
        compute_energy('files/fcc.mint', 'tempDir')

        # Check that everything went right
        self.assertTrue(os.path.isdir('tempDir/relaxation'))
        self.assertTrue(os.path.isfile('tempDir/relaxation/run_id'))

        # Copy in the results from first, unconverged, calculation
        shutil.rmtree('tempDir/relaxation')
        shutil.copytree('files/qmpy-test/relaxation_0', 'tempDir/relaxation')
        self.assertEqual(continue_energy_calculation('tempDir'), "In progress")

        # Copy in the results form the second, fully-converged, relaxation
        shutil.rmtree('tempDir/relaxation')
        shutil.copytree('files/qmpy-test/relaxation', 'tempDir/relaxation')

        # Start the static calculation
        continue_energy_calculation('tempDir')
        self.assertTrue(os.path.isdir('tempDir/static'))
        
        # Copy in static results, make sure energy comes out
        shutil.rmtree('tempDir/static')
        shutil.copytree('files/qmpy-test/static', 'tempDir/static')
        eng = compute_energy('files/fcc.mint', 'tempDir')
        self.assertTrue(abs(eng - -3.71268725 < 0.001))
    
    def test_convex_hull(self):
        compute_energy('files/fcc.mint', 'tempDir')
        shutil.copytree('files/qmpy-test/static', 'tempDir/static')
        e = get_stability('tempDir')
        self.assertTrue(e < 0.005)

    def tearDown(self):
        self.setUp()

        
        
if __name__ == '__main__':
    unittest.main()
