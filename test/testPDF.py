from fpassmgr.settings import Settings
from fpassmgr.pdf import PDFInterface
from qmpy import Structure
from qmpy.utils import parse_comp
import os
import tempfile
import unittest
import copy
import shutil

class UITester(unittest.TestCase):

    def setUp(self):
        Settings.pdf_dir = "./icdd-files/"
        Settings.work_dir = "./work-dir"
        Settings.template_dir = "../templates/"
        for x in [Settings.pdf_dir, Settings.work_dir]:
            if os.path.isdir(x): shutil.rmtree(x)
            os.mkdir(x)

    def tearDown(self):
        for x in [Settings.pdf_dir, Settings.work_dir]:
            if os.path.isdir(x): shutil.rmtree(x)

    def test_suitable(self):
        result, reason, atom_count = PDFInterface.pdf_entry_is_suitable('files/icdd-test/pattern570837.cif')
        self.assertFalse(result)
        self.assertTrue("partial occupancy" in reason)
        self.assertEqual(atom_count, 45.6)

        result, reason, atom_count = PDFInterface.pdf_entry_is_suitable('files/icdd-test/pattern630671.cif')
        self.assertFalse(result)
        self.assertTrue("Too many atoms" in reason)
        self.assertEqual(atom_count, 80.0)

        result, reason, atom_count = PDFInterface.pdf_entry_is_suitable('files/icdd-test/pattern630367.cif')
        self.assertFalse(result)
        self.assertTrue("known" in reason)
        self.assertEqual(atom_count, 18.0)

        result, reason, atom_count = PDFInterface.pdf_entry_is_suitable('files/icdd-test/pattern630306.cif')
        self.assertTrue(result)
        self.assertTrue("fine" in reason)
        self.assertEqual(atom_count, 36.0)

    def test_addition(self):
        path = PDFInterface.add_pdf_entry('files/icdd-test/pattern630306.cif')
        self.assertTrue(os.path.isfile(path))

    def test_get_problems(self):
        PDFInterface.add_all_pdf_entries('files/icdd-test/')
        unsolved = PDFInterface.get_unsolved_pdf_entries(number=5)
        self.assertEqual(len(unsolved), 5)
    
    def test_start_problem(self):
        PDFInterface.add_pdf_entry('files/icdd-test/pattern630306.cif')
        PDFInterface.start_entry('pattern630306')
        self.assertTrue(has_been_created('pattern630306'))

if __name__ == '__main__':
    unittest.main()
