from fpassmgr.settings import Settings
from fpassmgr.manager import *
from fpassmgr.web import *
from fpassmgr.validation import FPASSValidation
from qmpy import Structure
from qmpy.utils import parse_comp
import os
import tempfile
import unittest
import copy
import shutil

class UITester(unittest.TestCase):

    def setUp(self):
        Settings.debug = True
        Settings.website_dir = "./website"
        Settings.work_dir = "./work-dir"
        Settings.template_dir = "../files/"
        for x in [Settings.work_dir, Settings.website_dir]:
            if os.path.isdir(x): shutil.rmtree(x)
            os.mkdir(x)

    def test_website(self):
        ## Pretend a run has finished
        name = UI.prepare_solution('files/rutile.mint', 'files/rutile.xray')
        fpass_dir = get_solution_directory(name)
        for i in range(10):
            shutil.copytree('files/complete-run-example', os.path.join(fpass_dir, str(i)))
        FPASSValidation.start_validation(name)
        shutil.rmtree(FPASSValidation.get_validation_directory(name))
        shutil.copytree('files/validation-example/', FPASSValidation.get_validation_directory(name))

        ## Make website
        WebsiteMaker.make_website()

    def test_voldiff_plot(self):
        WebsiteMaker._write_voldiff_figure('voldiff.png',-.05)

    def test_stability_plot(self):
        WebsiteMaker._write_stability_figure('stability.png',-5)

    def test_xray_plot(self):
        WebsiteMaker._write_diffraction_pattern('files/Y3NiAl3Ge2.mint', 'files/Y3NiAl3Ge2.xray', 'x-ray.png')

    def test_pareto_plot(self):
        points = [[-5, 0.1], [-4.9, 0.3], [-5.1, 0.5]]
        WebsiteMaker._write_pareto_plot(points, 'pareto.png')

if __name__ == '__main__':
    unittest.main()

