from fpassmgr.torque import *
from fpassmgr.manager import *
import os
import shutil
import unittest

class UITester(unittest.TestCase):

    def test_qstat(self):
        result = torque.get_job_status(1)
        self.assertEqual(result, "Complete")
        result = torque.get_job_status(551689) # Replace with running job ID
        self.assertEqual(result, "Running")

    def test_manager(self):
        status = get_solution_statuses('O2Ti1-136') # Replace with name of current job
        self.assertEqual(status['0'], "Complete") # Replace with status of a job

    def test_job_status(self):
        statuses = get_all_job_statuses()
        self.assertTrue(len(statuses) > 1)

if __name__ == '__main__':
    unittest.main()
