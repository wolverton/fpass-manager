from fpassmgr.prototypes import *
from fpassmgr.mint import Mint
from qmpy import Structure
from qmpy.utils import parse_comp
import os
import tempfile
import unittest
import copy
import shutil

class UITester(unittest.TestCase):

    def test_import_file(self):
        x = PrototypeStructure.import_file('files/fcc.mint')
        self.assertEqual(x.name, 'Cu1-225')
    
    def test_prototype_match(self):
        self.assertTrue(prototypes_are_equal('files/b2.mint','files/b2.mint'))
        self.assertFalse(prototypes_are_equal('files/b2.mint','files/fcc.mint'))
        
    def test_explode_structure(self):
        temp = tempfile.mkstemp()[1]
        Mint.convert('files/b2.mint', 'vasp', filename=temp)
        x = io.poscar.read(temp)
        os.remove(temp)
        equiv = PrototypeStructure.generate_equivalent_structures(x)
        self.assertNotEqual(equiv[0].atoms[0].element, equiv[1].atoms[0].element)
    
    def test_save_and_load(self):
        x = PrototypeStructure.import_file('files/fcc.mint')
        path = x.save('.')
        y = PrototypeStructure.import_file(path)
        os.remove(path)
        self.assertTrue(x.is_equivalent_to(y))
    
    def test_factory(self):
        x = Structure.objects.all()[:10]
        library = PrototypeLibraryFactory.get_unique_prototypes(x)

    def test_save_load(self):
        x = Structure.objects.all()[:2]
        library = PrototypeLibraryFactory.get_unique_prototypes(x)
        PrototypeLibraryFactory.save_to_directory(library, 'library')
        new = PrototypeLibraryFactory.load_from_directory('library')
        self.assertEqual(len(library), len(new))
        shutil.rmtree('library')

    def test_formula(self):
        formula = 'AB'
        library = {}
        x = PrototypeStructure.import_file('files/fcc.mint')
        PrototypeLibraryFactory.add_prototype(library, x)
        x = PrototypeStructure.import_file('files/b2.mint')
        PrototypeLibraryFactory.add_prototype(library, x)
        self.assertEqual(len(library), 2)
        ablib = PrototypeLibraryFactory.get_applicable_prototypes(library, formula)
        self.assertEqual(len(ablib), 1)

    def test_instantiation(self):
        x = PrototypeStructure.import_file('files/b2.mint')
        new = x.create_instances('NiAl')
        self.assertEqual(len(new),1)
        self.assertEqual(new[0].comp, parse_comp('NiAl'))

    def test_library_lookup(self):
        ## Make library
        library = {}
        x = PrototypeStructure.import_file('files/fcc.mint')
        PrototypeLibraryFactory.add_prototype(library, x)
        x = PrototypeStructure.import_file('files/b2.mint')
        PrototypeLibraryFactory.add_prototype(library, x)

        ## Get match
        p, e = PrototypeLibraryFactory.find_matching_prototype(library, 'files/b2.mint')
        self.assertEqual(p.formula, 'AB')
        self.assertEqual(len(e), 1)

    def test_print(self):
        ## Make library
        library = {}
        x = PrototypeStructure.import_file('files/fcc.mint')
        PrototypeLibraryFactory.add_prototype(library, x)
        x = PrototypeStructure.import_file('files/b2.mint')
        PrototypeLibraryFactory.add_prototype(library, x)

        ## Print it out
        temp = tempfile.NamedTemporaryFile()
        PrototypeLibraryFactory.print_library(library, temp.name)

        ## Make sure it checks out
        fp = open(temp.name, 'r')
        for line in fp:
            words = line.split()
            self.assertTrue(words[0] in words[1])
        
if __name__ == '__main__':
    unittest.main()
