from fpassmgr.user import UI
from fpassmgr.settings import Settings
from fpassmgr.manager import *
import os
import shutil
import unittest

class UITester(unittest.TestCase):

    def setUp(self):
        Settings.debug = True    
        # Delete any directories that would be created during test
        for test in ['Br1Cu2-225']:
            simplePath = get_problem_directory(test)
            if os.path.isdir(simplePath):
                shutil.rmtree(simplePath)
    
    def test_preparer(self):
        # Test with a simple model
        name = UI.prepare_solution('files/simple.mint')
        self.assertEqual('Br1Cu2-225',name,"Unexpected solution name")
        # Make sure it exists and has a solution directory
        self.assertTrue(has_been_created(name))
        self.assertTrue(os.path.isdir(os.path.join(get_problem_directory(name),'fpass')))
        # Make sure it fails if you try to create it again
        self.assertRaises(Exception, UI.prepare_solution, 'files/simple.mint')
        UI.prepare_solution('files/simple.mint', override=True)
        
    def test_submission(self):
        # Create directory
        name = UI.prepare_solution('files/simple.mint')
        # Submit 1 job
        UI.start_solution(name, 1)
        self.assertTrue(os.path.isdir(get_calculation_directory(name,0)))
        self.assertEqual(number_solutions_started(name),1)
        # Submit 10 jobs
        UI.start_solution(name, 10)
        self.assertEqual(number_solutions_started(name),11)
        
    def test_status_checker(self):
        self.assertEquals("Not yet initialized", UI.check_status('Br1Cu2-225'))
        name = UI.prepare_solution('files/simple.mint')
        self.assertTrue("No solutions started" in UI.check_status(name))
        UI.start_solution(name,10)
        self.assertTrue("10 solutions started" in UI.check_status(name))
        # Test solution checker
        dir = get_calculation_directory(name, 0)
        fp = open(os.path.join(dir, 'best.mint'), 'w'); fp.close()
        self.assertTrue("1 solutions completed" in UI.check_status(name))
        self.assertTrue("9 solutions failed" in UI.check_status(name))
        
        
if __name__ == '__main__':
    unittest.main()
