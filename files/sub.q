#! /bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l walltime=250:00:00
#PBS -j oe
#PBS -N #NAME#

# System-specific variables
NPROCS=`cat $PBS_NODEFILE | wc -l`
WORKDIR=$PBS_O_WORKDIR
RUNDIR=$WORKDIR
MINT=/home/ltw578/software/mint/mint

# Link to run directory
cd $WORKDIR
if [ $WORKDIR != $RUNDIR ]; then
	ln -sf $RUNDIR tmpdir
fi

# Copy run files to run directory
if [ -e ../../xray.in ]; then XRAY="../../xray.in"; fi
for file in set.in ../input.strc pot.in pot.in; do
   cp ../$file $RUNDIR
done


# Run MINT
cd $RUNDIR
echo "Running on $NPROCS processors"
$MINT set.in input.strc pot.in $XRAY $FIXED -disp all -opt -n $NPROCS -relax -energy -print mint -name best -time >& stdout

# Handle results
if [ $WORKDIR != $RUNDIR ]; then
	# Copy results back, leave everything else in work directory 
	for output in stdout best.mint OPT_STR_1; do
	   cp -r $output $PBS_O_WORKDIR
	done
fi
