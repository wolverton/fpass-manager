from qmpy import FormationEnergy
import numpy as np

# Get all stabilities of ICSD compounds
icsd_stab = FormationEnergy.objects.filter(entry__path__contains = 'icsd').filter(
        fit='standard',stability__isnull = False).values_list('stability', flat=True)

# Get the volume of all finished ICSD calculations
icsd_vol, relax_vol = zip(*FormationEnergy.objects.filter(entry__path__contains = 'icsd').filter(fit='standard',
        stability__isnull = False).values_list('entry__structure__volume_pa', 'calculation__output__volume_pa'))
icsd_vol_change = [(x-y)/x for x,y in zip(icsd_vol, relax_vol) if x is not None and y is not None]
icsd_stab = [ z for x,y,z in zip(icsd_vol, relax_vol, icsd_stab) if x is not None and y is not None]
print 'Extracted %d stabilities'%len(icsd_stab)
print 'Extracted %d volume changes'%len(icsd_vol_change)

# Compute the histogram
stab_hist, stab_bins = np.histogram(icsd_stab, bins = np.linspace(-0.25, 0.25, 21))
stab_hist = stab_hist.astype(np.double)
stab_hist /= len(icsd_stab)

# Save the histogram
fp = open('stability.hist', 'w')
print >>fp, "bin_left", "frac_icsd"
for b,v in zip(stab_bins, stab_hist):
    print >>fp, b, v
fp.close()

# Compute the histogram
vol_hist, vol_bins = np.histogram(icsd_vol_change, bins = np.linspace(-0.4, 0.4, 21))
vol_hist = vol_hist.astype(np.double)
vol_hist /= sum(vol_hist)

# Save the histogram
fp = open('vol-change.hist', 'w')
print >>fp, "bin_left", "frac_icsd"
for b,v in zip(vol_bins, vol_hist):
    print >>fp, b, v
fp.close()

# Compute the percentiles
print 'Stability statistics:'
stable_iqr = np.percentile(icsd_stab, 75) - np.percentile(icsd_stab, 25)
print '\tMedian:', np.percentile(icsd_stab, 50)
print '\tIQR:', stable_iqr
for p in [80, 90, 95]:
    print '\t%d%% percentile:'%p, np.percentile(icsd_stab, p)
print '\t2 IQR cutoff:', np.percentile(icsd_stab, 50) + 2 * stable_iqr

print 'Volume change statistics'
print '\tMedian:', np.percentile(icsd_vol_change, 50)
vol_iqr = np.percentile(icsd_vol_change, 75) - np.percentile(icsd_vol_change, 25)
print '\t25/75th percentiles:', np.percentile(icsd_vol_change,25), np.percentile(icsd_vol_change,75)
print '\tIQR:', vol_iqr
print '\tBounds:', np.percentile(icsd_vol_change, 50) - vol_iqr * 2, \
    np.percentile(icsd_vol_change, 50) + vol_iqr * 2
