#PBS -N vasp
#PBS -l nodes=1:ppn=8
#PBS -l walltime=48:00:00
#PBS -m a

cd $PBS_O_WORKDIR
NPROCS=`wc -l < $PBS_NODEFILE`
cat $PBS_NODEFILE > nodes
mpirun -np $NPROCS -machinefile $PBS_NODEFILE /usr/local/bin/vasp_53 > vasp.out
