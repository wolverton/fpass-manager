'''
Operations that fail to fall into a neat category

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''

def xray_data_is_processed(path):
    '''
    Determine whether the X-ray data contained in a file is processed or not
    :param path: Path to x-ray data file
    :return: Whether peaks are integrated
    '''
    from numpy.lib.function_base import diff

    ## Read in the entire file
    fp = open(path, 'r')
    lines = fp.read().split("\n")
    fp.close()

    ## See if the first line reads XML (these are processed patterns from the PDF)
    if "xml" in lines[0]: return True

    ## Get the angles
    angles = []
    for line in lines:
        words = line.split()
        if len(words) < 2: continue
        try:
            angle = float(words[0])
        except ValueError:
            continue
        else:
            angles.append(angle)

    ## See if the peak count as absurd
    if len(angles) > 500: return False

    ## Get difference between angles, see if any two spacings are more than 10% different in size
    angles = sorted(set(angles))
    diffs = diff(angles)
    return max(diffs) / min(diffs) > 1.1

def get_xray_pattern(path):
    '''
    Retrieve an x-ray pattern from file.

    Note: This does not currently support XML files from PDF4+
    :param path: Path to x-ray file
    :return: 2D array: [angle, intensity]
    '''

    #
    fp = open(path, 'r')
    lines = fp.read().split("\n")
    fp.close()

    ## See if the first line reads XML (these are processed patterns from the PDF)
    if "xml" in lines[0]:
        raise Exception("XML files are not currently supported")

    ## Read in the file
    angles = []
    intens = []
    for line in lines:
        words = line.split()
        if len(words) < 2: continue
        try:
            angle = float(words[0])
            inten = float(words[1])
        except ValueError:
            continue
        else:
            angles.append(angle)
            intens.append(inten)

    return zip(angles, intens)
