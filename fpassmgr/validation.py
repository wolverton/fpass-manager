'''
Operations used for controlling validation of candidate solutions
from either FPASS or database searching.

Two classes and how to use them:

1. Validation - Validate structure regardless of origin
    a. Checks energy compared to hull, volume change, etc. (see validate_solution)
    b. Start validation by calling Validation.start_validation(strc)
        i. Begins the process of running DFT calculation
    c. Run by calling Validation.validate_solution(strc, goal)
        i. Keep going until it returns something besides "In progress"
2. FPASSValidation - Validate FPASS solutions
    a. Does everything from Validation, plus a few things.
    b. Get FPASS result by calling FPASSValidation.get_optimal_solution(name)
    c. If that is complete, start validation following the same commands as Validate, but replace Validation with FPASSValidation
        

Author:
    Logan Ward (ward.logan.t@gmail.com)
'''

from tempfile import NamedTemporaryFile
from qmpy import io
from qmpy.analysis.symmetry.spacegroup import Spacegroup
from fpassmgr.dscsp import DSCSP
from fpassmgr.prototypes import prototypes_are_equal
from fpassmgr.qmpywrap import compute_energy, get_stability
from fpassmgr.util import xray_data_is_processed
from fpassmgr.manager import *
from fpassmgr.mint import *

'''
Holds operations for validating any structure, regardless of origin. See module documentation for how to use this class.
'''
class Validation:

    @staticmethod
    def get_status(strc):
        '''
        Determine status of validation.

        Possible statuses:
            Not started
            In progress
            Failed
            Complete

        :param strc: Path to structure being validated (absolute)
        :return: String describing status
        '''

        if not os.path.isfile(strc):
            raise Exception("No structure found: " + strc)

        strc_dir = os.path.dirname(strc)
        calc_dir = os.path.join(strc_dir, "calc")

        ## Check if energy calculation has been started
        if not os.path.isdir(calc_dir):
            return "Not started"

        ## See if the energy calculation has completed
        energy = compute_energy(strc, calc_dir)
        if energy == "In progress":
            return energy
        if energy == "FAILED":
            return "FAILED: DFT calculation has crashed"
        return "Complete"

    @staticmethod
    def validate_solution(strc, goal, details=False):
        '''
        Call to either get validation energy or continue the process.

        Validation methods:
            1. Compare energy to OQMD convex hull
            2. Check whether specific volume is much different from experimental values
            3. Check whether the spacegroup is correct
        
        Possible statuses:
            Not started - Validation not started
            In progress - Validation steps in progress
            <validation result> - Describing what has been found

        Result format:
            [Check] <...> - Excellent confirmation of solution
            <...> - Passable confirmation
            [Warning] - Solution should be viewed with suspiciion

        :param strc: Path to structure being validated (absolute)
        :param goal: Mint-formatted structure describing solution problem
        :param details: Whether to calculate/determine extended details about each step
        :return String describing energy. If details, also returns a dict mapping step name to some kind of detail about
        the result (see documentation for each validation method).
        '''


        ## Get the status of this validation
        status = Validation.get_status(strc)
        if not "Complete" in status:
            return (status, {}) if details else status

        ## Get directories
        strc_dir = os.path.dirname(strc)
        calc_dir = os.path.join(strc_dir, "calc")

        ## If complete, collect results
        output = ""

        detail_dict = {}
        # Get the energy results
        if details:
            temp, detail = Validation._assess_stability(calc_dir, details)
            detail_dict['stability'] = detail
            output += temp
        else:
            output += Validation._assess_stability(calc_dir)

        target_volume, target_spg = Validation._get_goal_structure(goal)
        # Get the volume change result
        if details:
            temp, detail = Validation._assess_volume_change(calc_dir, target_volume, True)
            output += temp
            detail_dict['volume'] = detail
        else:
            output += Validation._assess_volume_change(calc_dir, target_volume)

        # Get the space groups
        output += Validation._assess_space_group(calc_dir, target_spg)

        if details:
            return output, detail_dict
        else:
            return output

    @staticmethod
    def _assess_stability(calc_dir, details=False):
        '''
        Assess the stability of the structure as an indication of how realistic it is

        Reasoning:
            Median stability of ICSD compounds in OQMD: -4.5 meV/atom
            IQR of stability of ICSD compounds: 66.0 meV/atom
            If something is within the 80% percentile (36.4 meV/atom), we'll call it acceptable

        :param calc_dir: Directory in which DFT calculations were run
        :param details: Whether to return detailed results
        :return: String describing validation result, [if details] stability in meV/atom
        '''

        stab = get_stability(calc_dir)
        if stab < 0.0364: # Under the 80% percentile
            output = "[Check] Solution is very energetically feasible."
        elif stab < 0.127: # Inside of 3 IQRs
            output = "Solution is kind of energetically feasible."
        else:
            output = "[Warning] Solution is energetically unfeasible."
        output += " Stability = %.4f meV/atom"%(stab * 1000.0)

        if not details:
            return output
        else:
            return output, stab * 1000.0

    @staticmethod
    def _assess_volume_change(calc_dir, target_volume, details=False):
        '''
        Assess the stability of the structure

        Reasoning:
            50% of all ICSD compounds are between -3.9% and 0% volume change on relaxation in DFT
            Let's assume that a compound that relaxes within that range not "certainly wrong"
            If it is an 2 * IQR outlier (below -8.1% or above 7.9% change), that is worrisome

        :param calc_dir: Directory in which DFT calculations were run
        :param target_volume: Volume/atom of goal structure
        :param details: Whether to return the fractional volume change
        :return: String describing validation result. If details, also returns the volume difference
        '''
        relaxed_strc = os.path.join(calc_dir, "static", "POSCAR")
        vdiff = (io.poscar.read(relaxed_strc).volume_pa - target_volume) / target_volume
        if vdiff < 0 and vdiff > -0.039:
            output = "\n[Check] Volume agreement is excellent."
        elif vdiff < 0.079 and vdiff > -0.081:
            output = "\nVolume difference is in acceptable range."
        else:
            output = "\n[Warning] Volume change on relaxation is rather large."
        output += " Vdiff = %.2f %%"%(vdiff * 100)

        if details:
            return output, vdiff
        else:
            return output

    @staticmethod
    def _assess_space_group(calc_dir, target_spg):
        '''
        Assess the space group of final structure
        :param calc_dir: Directory in which DFT calculations were run
        :param target_spg: Spacegroup number of target structure
        :return:
        '''
        relaxed_strc = os.path.join(calc_dir, "static", "POSCAR")
        found_spg = Mint.get_space_group(relaxed_strc, tol=0.01)
        if target_spg == found_spg:
            output = "\n[Check] Space groups agree."
        else:
            t = Spacegroup.get(target_spg)
            f = Spacegroup.get(found_spg)
            if t.lattice_system == f.lattice_system:
                output = "\nDifferent space group, same system. Target: %d, Found %d"%(target_spg, found_spg)
            else:
                output = "\n[Warning] Different space group and system. Target: %d, Found %d"%(target_spg, found_spg)
        return output

    @staticmethod
    def _get_goal_structure(goal):
        '''
        Get some statistics about the goal structure

        :param goal: Path to goal structure (Mint format)
        :return: Volume per atom, spacegroup of goal object
        '''


        if not os.path.isfile(goal):
            raise Exception("No such file: " + goal)

        ## Creates a random solution for the goal structure
        fp = open(goal,'r')
        spg = -1
        for line in fp:
            if "space" in line: 
                try:
                    spg = int(line.split()[1])
                except:
                    raise Exception("Expected numerical space group. Found: " + line)
        if spg == -1:
            raise Exception("Spacegroup not found!")

        ## Import it as QMPY structure (to calculate volume_pa)
        temp = NamedTemporaryFile()
        Mint.convert(goal, 'vasp', temp.name)
        strc = io.poscar.read(temp.name)
        return strc.volume_pa, spg

    @staticmethod
    def start_validation(strc):
        '''
        Begin the validation process.

        :param strc: Path to structure being validated
        '''

        if not os.path.isfile(strc):
            raise Exception("No structure found: " + strc)

        strc_dir = os.path.dirname(strc)
        calc_dir = os.path.join(strc_dir, "calc")

        if os.path.isdir(calc_dir):
            raise Exception("Validation has already been started: " + strc)

        os.mkdir(calc_dir)
        compute_energy(strc, calc_dir)


'''
Tasks specific to validating FPASS solutions
'''
class FPASSValidation:
    ''' Minimum number of times a solution needs to complete for agreement to be valid '''
    min_quorum = 10
    ''' Minimum agreement for a solution to be declared valid '''
    min_agreement = 5

    @staticmethod
    def get_optimal_solution(name):
        '''
        Get the optimal FPASS solution. For now, this is defined as
        the lowest-energy solution.

        :param name Identifying name of FPASS problem
        :return Path to best structure, number in agreement, energy of structure.
            None if quorum / agreement has not been reached.
        '''

        if not has_been_created(name):
            return None

        quorum = 0
        agreement = 0
        min_energy = 1e100
        best_structure = None
        ## Find the lowest-energy structure
        status = get_solution_statuses(name)
        for run in status.keys():
            if status[run] != "Complete": continue
            strc, energy = get_solution(name,run)
            quorum += 1
            if min_energy > energy :
                best_structure = strc
                min_energy = energy

        ## See how many agree
        for run in status.keys():
            if status[run] != "Complete": continue
            strc, energy = get_solution(name,run)
            if Mint.compare(strc, best_structure, tol=0.1):
                agreement += 1

        if quorum < FPASSValidation.min_quorum:
            best_structure = None
        if agreement < FPASSValidation.min_agreement:
            best_structure = None
        return best_structure, agreement, min_energy

    @staticmethod
    def get_validation_directory(name):
        '''
        Get the directory in which validation calculations will be run
        :param name: Name of FPASS problem
        :return: Path to the validation directory
        '''
        fpass_dir = get_solution_directory(name)
        val_dir = os.path.join(fpass_dir, "validation")
        return val_dir

    @staticmethod
    def get_goal_file(name):
        '''
        Get path to goal file (input.strc)

        :param name: Name of FPASS problem
        :return Path to file describing the target structure
        '''
        return os.path.join(get_problem_directory(name), "input.strc")

    @staticmethod
    def get_xray_file(name):
        '''
        Get path to xray file
        :param name: Name of FPASS problem
        :return: Path to x-ray file. None if no XRD data was provided
        '''
        file = os.path.join(get_problem_directory(name), 'xray.in')
        if os.path.isfile(file): return file
        else: return None

    @staticmethod
    def start_validation(name):
        '''
        Start the process of validating an FPASS solution, if appropriate quorum/agreement has been reached

        :param name: Identifying name of FPASS problem
        '''

        if not has_been_created(name):
            raise Exception("No such FPASS problem" + name)

        ## Check that solution has been agreed upon
        best_strc, agreement, min_energy = FPASSValidation.get_optimal_solution(name)
        if best_strc is None:
            raise Exception("No solution has been agreed upon yet for " + name)

        ## Make a validation directory
        val_dir = FPASSValidation.get_validation_directory(name)
        if os.path.exists(val_dir) :
            raise Exception('Validation has already been started for ' + name)
        os.mkdir(val_dir)

        ## Write the best solution into that directory in VASP format
        strc_to_val = os.path.join(val_dir, 'best.vasp')
        Mint.convert(best_strc, 'vasp', filename=strc_to_val)

        ## Call Validation to begin those tests
        Validation.start_validation(strc_to_val)

    @staticmethod
    def get_status(name):
        '''
        Determine status of validation.

        Possible statuses:
            Not eligible : Not enough FPASS calculations have completed/agreed to start the validation
            Not started : Validation process has not been started yet
            Different started : Validation process has been started, but with a different solution (up to you to fix this)
            In progress : Some calculations are still underway
            Complete

        :param name: Name of FPASS problem
        :return: String describing status of problem
        '''

        ## See if quorum / agreement has been reached
        best_strc, agreement, energy = FPASSValidation.get_optimal_solution(name)
        if best_strc is None:
            return "Not eligible"

        ## See if the validation has been started
        val_dir = FPASSValidation.get_validation_directory(name)
        if not os.path.isdir(val_dir):
            return "Not started"

        ## Ensure that the optimal structure has not changed
        to_validate = os.path.join(FPASSValidation.get_validation_directory(name), 'best.vasp')
        if not Mint.compare(best_strc, to_validate):
            return "Different started"

        ## Get the status of basic validation tests
        return Validation.get_status(to_validate)

    @staticmethod
    def validate_solution(name, details=False):
        '''
        Either get the status of validation, or get the solution.

        In addition to validation steps described in Validation.validate_solution. Performs the following:
            1. If X-ray data:
                a. Compare X-ray match to pattern (R Factor < <TBD threshold>)
                b. See whether lowest energy structure has best match
            2. Checks solution against prototypes
                a. Do any of them match this solution (same prototype)?
                b. If energies have been calculated: How do the energies compare?

        Returns the following statuses:
            Not eligible : Not enough FPASS calculations have completed/agreed to start the validation
            Not started : Validation process has not been started yet
            Different started : Validation process has been started, but with a different solution (up to you to fix this)
            In progress : Some calculations are still underway
            <validation result> : All checks have completed, returns current validation result

        :param name: Identifying name of FPASS problem
        :param details: Whether to return dictionary of results
        :return: Validation status / result
        '''

        ## Get the status of validation
        status = FPASSValidation.get_status(name)

        ## Check if we are done
        if not "Complete" in status:
            if details: return status, {}
            else: return status

        ## Run the generic validation tests
        goal = FPASSValidation.get_goal_file(name)
        to_validate = os.path.join(FPASSValidation.get_validation_directory(name), 'best.vasp')
        if details:
            status, detail_dict = Validation.validate_solution(to_validate, goal, details=True)
        else:
            status = Validation.validate_solution(to_validate, goal)

        ## Run the FPASS-specific ones
        output = status

        # ... pertaining to XRD
        xray = FPASSValidation.get_xray_file(name)
        if not xray is None:
            if details:
                temp, detail = FPASSValidation._assess_rFactor(name, to_validate, details=True)
                detail_dict['r'] = detail
                output += "\n" + temp
                temp, detail = FPASSValidation._assess_pareto(name, details=True)
                detail_dict['pareto'] = detail
                output += "\n" + temp
            else:
                output += "\n" + FPASSValidation._assess_rFactor(name, to_validate)
                output += "\n" + FPASSValidation._assess_pareto(name)

        # ... comparing to prototype match
        output += "\n" + FPASSValidation._assess_prototype_match(name, to_validate)

        if details:
            return output, detail_dict
        else:
            return output

    @staticmethod
    def _assess_rFactor(name, strc, details=False, read_last_result = True):
            '''
            Assess the match between the proposed structure and the known x-ray pattern (if available)

            :param name: Identifying name of FPASS problem
            :param strc: Structure being validated
            :param details: Whether to save X-ray pattern
            :param read_last_result: Whether the read the last result from disk, if available
            :return: String describing the X-ray match, [if details] x-ray pattern
            '''

            ## Get xray data file
            xray = FPASSValidation.get_xray_file(name)
            if xray is None:
                return "No XRD data provided" if not details else ("No XRD data provided", [])

            ## Potential shortcut: Read last result from disk
            last_result = os.path.join(os.path.dirname(strc), 'diffraction.out')
            rietveld = not xray_data_is_processed(xray)
            if read_last_result and os.path.isfile(last_result):
                fp = open(last_result, 'r')
                rFactor = float(fp.readline().split()[1])
                pattern = [ [ float(x) for x in line.split() ] for line in fp ]
                fp.close()

            ## Get R factor the long way
            else:
                rFactor, pattern = Mint.get_xray_match(strc, xray, refine=True, rietveld=rietveld, get_pattern=True)
                if read_last_result:
                    fp = open(last_result, 'w')
                    print >>fp, "RFactor %.8e"%(rFactor)
                    for line in pattern:
                        print >>fp, " ".join([ "%.6f"%x for x in line ])
                    fp.close()

            ## Assess match in words
            if rFactor < 0.1: output = "[Check] Excellent match to x-ray pattern."
            elif rFactor < 0.25: output = "Decent match to x-ray pattern."
            else: output = "[Warning] Poor match to x-ray pattern."
            if rietveld: output += " Rp factor: %.4f"%(rFactor)
            else: output += " R factor: %.4f"%(rFactor)

            return output if not details else (output, pattern)

    @staticmethod
    def _assess_pareto(name, details=False):
        '''
        Check whether the minimum-energy structure is also the best match to XRD data
        :param name: Name of FPASS problem
        :param details: Whether to output points on pareto plot
        :return: String describing validation result
        '''

        ## Get the status of each solution
        statuses = get_solution_statuses(name)

        ## Find the lowest-energy and best-matching results from all searches
        lowest_eng = {'e':1e100, 'r':0}
        best_match = {'e':0, 'r':1e100}
        energies = []
        matches = []
        for run, status in statuses.iteritems():
            if status != "Complete": continue
            path = os.path.join(get_calculation_directory(name, run))
            # Open fitness file
            fp = open(os.path.join(path, 'OPT_STR_1', 'OPT_1', 'fitness.out'))
            while True:
                line = fp.readline()
                if "Generation" in line:
                    fp.readline()
                    while True:
                        line = fp.readline().split()
                        if len(line) < 1: break
                        e = float(line[1])
                        r = float(line[2])
                        energies.append(e)
                        matches.append(r)
                        if e < lowest_eng['e']:
                            lowest_eng['e'] = e
                            lowest_eng['r'] = r
                        if r < best_match['r']:
                            best_match['e'] = e
                            best_match['r'] = r
                    continue
                if line == '': break

        ## Analyze differences
        strc_to_val = os.path.join(FPASSValidation.get_validation_directory(name), 'best.vasp')
        natoms = io.poscar.read(strc_to_val).natoms
        diffr = lowest_eng['r'] - best_match['r']
        diffe = (best_match['e'] - lowest_eng['e']) / natoms * 1000
        if diffr < 1e-3:
            output = "[Check] No structure matches XRD pattern better than lowest eng structure."
        elif diffe > 100:
            output = "There is a better matching structure, but it is much higher in energy."
        else:
            output = "[Warning] There is a better matching structure, and it is low in energy."
        output += " dR = %.3e, dE = %.3f meV/atom"%(diffr, diffe)

        ## Adjust the energies to be eV/atom
        for i in range(len(energies)):
            energies[i] -= lowest_eng['e']
            energies[i] /= float(natoms) / 1000.0

        return output if not details else (output, zip(energies, matches))

    @staticmethod
    def _assess_prototype_match(name, strc):
        '''
        Assess whether any already-known structures match this candidate.

        NOTE: This will fail if strc has different spacegroup than target.

        :param name: Name of FPASS problem
        :param strc: Path to structure being validated
        :return: String describing validation
        '''

        p_dir = get_prototype_directory(name)

        ## If prototypes have not already been gathered, do so now
        if not os.path.isdir(p_dir):
            DSCSP.gather_matching_prototypes(name)

        ## See if any of the prototypes match this solution
        names, paths = DSCSP.candidate_prototypes(name)
        for name, path in zip(names,paths):
            try: 
                if prototypes_are_equal(path,strc):
                    return "[Check] Prototype is already known. Example: " + name
            except:
                print >>sys.stderr, "Check failed for prototype at %s"%path
                continue
        return "Brand new structural prototype (at least for OQMD)"

