'''
Wrapper around QMPY-based methods

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''

from mint import *
from fpassmgr.torque import *
from qmpy import Calculation, Structure, io, PhaseSpace, PhaseData
from qmpy.utils import *
import os
import tempfile 
import shutil

def compute_energy(strc, directory, nnodes=1, nprocs=4, settings={}):
    '''
    Given a structure and path, compute the energy using OQMD settings.

    See continue_energy_calculation for how this is done.

    :param strc Structure in any format readible by mint, or a qmpy Structure object
    :param directory Directory in which to store calclulations
    :param nnodes Number of nodes to use in calculation
    :param nprocs Number of processors per node
    :return Energy, if calculations have been finished. If not, returns either "In progress" or "FAILED"
    '''
    
    is_file = isinstance(strc, str) and os.path.isfile(strc)
    is_qmpy = isinstance(strc, Structure)
    if not (is_file or is_qmpy):
        raise Exception("strc must be a file or qmpy object")
    if not os.path.isdir(directory):
        raise Exception("No such directory: " + directory)

    ## See whether this calculation has been started
    poscar_path = os.path.join(directory, "POSCAR.initial")
    if os.path.isdir(os.path.join(directory, "relaxation")):
        # Check whether the POSCAR in initialize matches this structure
        if not Mint.compare(strc, poscar_path):
            raise Exception("Calculation already started with different structure in: " + directory)
        # If so, continue as planned
        status = continue_energy_calculation(directory)
        return status

    ## Convert structure to POSCAR format, store in root directory
    if is_file:
        Mint.convert(strc, 'vasp', filename=poscar_path)
    else:
        io.poscar.write(strc, poscar_path)

    ## Write settings to output file
    set_file = os.path.join(directory, 'vasp.settings')
    fp = open(set_file, 'w')
    for k,v in settings.iteritems():
        print >>fp, k, v
    fp.close()

    ## Create a QSUB file specific to this problem
    shutil.copy(os.path.join(Settings.template_dir,"vasp.q"), directory) 
    queue_file = os.path.join(directory, 'vasp.q')
    edit_queue_file(queue_file, proc=[nnodes,nprocs])
    
    ## Start calculation
    # Call QMPY to generate VASP files
    if is_file:
        strc_obj = io.poscar.read(poscar_path)
    else:
        strc_obj = strc
    calc_dir = os.path.join(directory, 'relaxation')
    Calculation.setup(strc_obj, configuration='relaxation', path=calc_dir, settings=settings)

    # Handle submission
    qsub_in_dir('../vasp.q', calc_dir)

    return "In progress"

def continue_energy_calculation(directory):
    '''
    Continue an OQMD energy evaluation housed in a certain directory. 

    Starts with "relaxation", then performs "static" 

    Successive calls to this method will propogate those computations. Will not
    return with a energy until they have all be completed.

    :param directory Directory containing OQMD calculation
    :return Energy, if calculations have completed. If not, returns either "In progress" or "FAILED"
    '''

    if not os.path.isdir(directory):
        raise Exception('No such directory: ' + directory)
    if not os.path.isdir(os.path.join(directory, 'relaxation')):
        raise Exception('No calculation has been started in: ' + directory)

    ## Get status / continue VASP
    relax_dir = os.path.join(directory, "relaxation")
    relax_status = _run_vasp_calculation(relax_dir)
    if relax_status != "Complete":
        if relax_status == "FAILED": return "FAILED"
        else: return "In progress"

    ## Start static calculation if that has not already been started
    static_dir = os.path.join(directory, 'static')
    if not os.path.isdir(static_dir):
        # Get VASP settings
        set_file = os.path.join(directory, 'vasp.settings')
        fp = open(set_file, 'r')
        settings = {}
        for line in fp:
            words = line.split()
            if len(words) < 2: continue
            key = words[0]
            val = " ".join(words[1:])
            settings[key] = val
    
        # If not, start it
        relax = Calculation.read(relax_dir)
        contcar = relax.output
        chgcar = os.path.join(relax_dir, "CHGCAR")
        wavecar = os.path.join(relax_dir, "WAVECAR")
        Calculation.setup(contcar, configuration='static', path=static_dir, 
            chgcar=chgcar, wavecar=wavecar, settings=settings)

        # Handle submission
        qsub_in_dir('../vasp.q', static_dir)
        return "In progress"

    ## See if static has completed
    static_status = _run_vasp_calculation(static_dir)
    if static_status is "Complete":
        calc = Calculation.read(static_dir)
        return calc.energy_pa
    else: return static_status
    
def _run_vasp_calculation(directory):
    '''
    Given that VASP calculation is going on in a directory, keep running it and addressing errors until complete.

    Follows the procedure:
        1. Check if job in the directory has completed. If not, return "In progress"
        2. Check if the job in the directory failed to converged. If so,
            a. Import calculation, address errors
            b. If error addressing fails, return "FAILED"
            c. Otherwise, write out new calculation files and resubmit
        3. If job is finished and converged
            b. Return "Complete"

    :param directory Directory in which calculation is running
    :return Status message
    '''

    if not os.path.isdir(directory):
        raise Exception('No such directory')

    ## Check if complete
    calc_status = get_calculation_status(directory)
    if calc_status != "Complete": return "In progress"
        
    ## Check if converged
    if not (os.path.isfile(os.path.join(directory, "OUTCAR")) or
        os.path.isfile(os.path.join(directory, "OUTCAR.gz"))) and Settings.debug:
        return "In progress"
    calc = Calculation.read(directory)
    if calc.converged:
        #calc.compress()
        return "Complete"

    ## If not, address errors and resubmit
    try:
        new_calc = calc.address_errors()
    except:
        status = "FAILED"
    new_calc.path = calc.path # Ensure directory does not change
    new_calc.write()
    qsub_in_dir('../vasp.q', directory)

def get_stability(path, more_info = False):
    '''
    Get the stability of a structure, given the path to its calculation directory

    :param path Path to calculation directory (that holds both relaxation and static steps)
    :param more_info Whether to return reaction in HTML format
    :return Stability of structure in eV/atom. Returns None if static calculation is not complete. If more_info, returns formation energy and phases as well
    '''

    if not os.path.isfile(os.path.join(path, 'POSCAR.initial')):
        raise Exception('Not calculation directory')
    static_dir = os.path.join(path, 'static')
    if not os.path.isdir(static_dir):
        return None

    ## Read in calculation, compute delta_e
    calc = Calculation.read(static_dir)
    if not calc.converged: return None
    if 'compute_formation' in dir(calc):
        calc.compute_formation()
    else:
        calc.get_formation()

    ## Get stability energy
    data = PhaseData()
    data.load_oqmd(calc.input.comp.keys())
    ps = PhaseSpace(calc.input.comp, data = data)
    delta_e, phase = ps.gclp(calc.input.composition.unit_comp)

    ## Result
    output = calc.formation.delta_e - delta_e
    if output is None:
        raise Exception("Some problem with compute_formation")
    if not more_info:
        return output

    ## Redo calculation with full composition
    this_comp = reduce_comp(calc.input.comp)
    delta_e, phases =  ps.gclp(this_comp)
    reactants = " + ".join([ "%.2f %s"%(a,format_html(reduce_comp(p.comp))) for p, a in phases.items() if a > 1e-3])
    return output, calc.formation.delta_e, reactants
