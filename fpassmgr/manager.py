'''
Operations related to managing calcuations. Does things like:

    - Get paths to FPASS solution directories
    - Start FPASS calculations
    - Return status of individual calculations

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''

from fpassmgr.settings import *
from fpassmgr.torque import *
import os
import sys
import shutil
from qmpy import io, Structure
from tempfile import NamedTemporaryFile
from mint import *

def get_current_problems():
    '''
    Get list of all current FPASS problems
    :return List containing names of each current FPASS problem
    '''
    problems = []
    for x in os.listdir(Settings.work_dir):
        if os.path.isdir(os.path.join(Settings.work_dir,x)): 
            problems.append(x)
    return problems

def get_problem_directory(name):
    '''
    :param name Name of FPASS problem
    :return Path to the directory holds data about this problem
    '''
    return os.path.join(Settings.work_dir, name)
    
def get_solution_directory(name):
    '''
    :param name Name of FPASS problem
    :return Path to the directory holds the FPASS calculations themselves
    '''
    return os.path.join(get_problem_directory(name),'fpass')
    
def get_calculation_directory(name, number):
    '''
    :param name Name of FPASS problem
    :param number Index a particular FPASS calculation
    :return Path to a specific FPASS calculation
    '''
    return os.path.join(get_solution_directory(name), str(number))

def get_prototype_directory(name):
    '''
    Get the directory in which prototype structures are evaluated
    :param name: Name of FPASS problem
    :return: Prototype directory
    '''
    return os.path.join(get_problem_directory(name), 'prototypes')
    
def has_been_created(name):
    ''' 
    :param name Identifying name of an FPASS calculation
    :return Whether it has been initialized
    '''
    return os.path.isdir(get_problem_directory(name))

def get_target_spacegroup(name):
    '''
    Get target spacegroup of a current FPASS problem
    :param name: Name of FPASS problem
    :return: Number of space group in which FPASS is searching, or name if that is in the title
    '''

    if not has_been_created(name):
        raise Exception('No such FPASS problem: ' + name)
    strc = os.path.join(get_problem_directory(name), 'input.strc')
    fp = open(strc, 'r')
    spg_num = -1
    for l in fp:
        if "space" in l:
            try:
                spg_num = int(l.split()[1])
            except:
                return "".join(l.split()[1:])
    if spg_num == -1:
        raise Exception('Spacegroup not found in file: ' + strc)
    return spg_num

def get_example_solution(name):
    '''
    Randomly generate a solution to this problem (useful in getting target volumes/compositions)
    :param name: Name of FPASS problem
    :return: QMPY Structure object representing an arbitrary solution
    '''

    strc = os.path.join(get_problem_directory(name), 'input.strc')

    temp_path = NamedTemporaryFile()
    attempts = 0; # Sometimes generating a random structure fails
    obj = None
    while attempts < 10:
        try:
            Mint.convert(strc, "vasp", temp_path.name)
        except:
            print >>sys.stderr, 'Failed to make structure due to ' + str(sys.exc_info()[1])
            attempts += 1
            continue
        obj = io.poscar.read(temp_path.name)
        break
    if obj is None:
        raise Exception('Failed to make example solution for : ' + name)
    return obj
    
def initialize_problem_directory(name, strc, xray=None):
    '''
    Create a directory to store input files and results
    
    :param name Identifying name of an FPASS calculation
    :param strc Path to the Mint-formatted input structure
    :param xray Path to the X-ray data file
    '''
    # Check input
    if has_been_created(name):
        raise Exception('Calculation has already been created: ' + name)    
    if not os.path.isfile(strc):
        raise Exception('Structure file not found: ' + strc)
    if xray != None and not os.path.isfile(xray):
        raise Exception('Xray file not found ' + xray)
        
    # Create the directory
    dir_name = get_problem_directory(name)
    os.mkdir(dir_name)
    
    # Copy the input files
    shutil.copy(strc, os.path.join(dir_name, "input.strc"))
    if xray != None:
        shutil.copy(xray, os.path.join(dir_name, "xray.in"))
        
    # Create the FPASS run directory, and copy template files
    fpass_dir = get_solution_directory(name)
    os.mkdir(fpass_dir)
    for f in ["pot.in", "set.in", "sub.q"]:
        shutil.copy(os.path.join(Settings.template_dir, f), fpass_dir)
        
    # Replace the name of the jobs in sub.q
    fi = open(os.path.join(fpass_dir,'sub.q'), 'r')
    fo = open(os.path.join(fpass_dir,'temp.q'), 'w')
    for line in fi:
        if "#PBS -N" in line:
            line = "#PBS -N " + name[:14]
        print >>fo, line.rstrip()
    fi.close(); fo.close()
    shutil.move(os.path.join(fpass_dir,'temp.q'),
        os.path.join(fpass_dir,'sub.q'))
        
def start_fpass_solutions(name, number=1):
    '''
    Prepare and submit an FPASS calculation to the queue
    :param name Name of problems
    :param number Number of calculations to start
    '''
    
    if not has_been_created(name):
        raise Exception('Directory has not been intialized')
    fpass_dir = get_solution_directory(name)
    
    started = 0
    count = 0
    home_dir = os.getcwd()
    while started < number:
        calc_dir = get_calculation_directory(name, count)
        if not os.path.isdir(calc_dir):
            started += 1
            os.mkdir(calc_dir)
            os.chdir(calc_dir)
            qsub(os.path.join('..','sub.q'))
            os.chdir(home_dir)
        count += 1

def number_solutions_started(name):
    '''
    Determine the number of FPASS solutions that have been started
    
    :param name Name of problem
    :return Number of solutions
    '''
    calculation_path = get_solution_directory(name)
    
    ## Count the number of directories in here
    count = 0
    for path in os.listdir(calculation_path):
        if "validation" in path: continue
        if os.path.isdir(os.path.join(calculation_path,path)): 
            count += 1
    
    return count

def get_solution_statuses(name):
    ''' 
    Determine the status of each FPASS solution. See get_solution_status

    :param name Name of problem
    :return Dictionary of job number mapped to status
    '''

    calculation_path = get_solution_directory(name)

    ## Count the number of directories in here
    statuses = {}
    for path in os.listdir(calculation_path):
        if path == "validation": continue
        calc_dir = os.path.join(calculation_path,path)
        if os.path.isdir(calc_dir):
            statuses[path] = get_solution_status(name, path)
    return statuses

def get_solution_status(name, number):
    '''
    Get the status of a single FPASS iteration. Three possible statuses:
        Running: Either in queue or currently underway
        Complete: Solution has finished, returning a solution
        Failed: Job is no longer running, no solution returned

    :param name Identifying name of FPASS problem
    :param number Iteration number
    :return Status of that run (see get_solution_statuses)
    '''

    if not has_been_created(name):
        raise Exception("Problem has not initialized: " + name)
    calc_dir = get_calculation_directory(name,number)
    if not os.path.isdir(calc_dir):
        raise Exception("Calculation has not been started: %s:%s"%(name,str(number)))
    
    # Get run ID
    status = get_calculation_status(calc_dir)

    # Manage appropriately
    if "Complete" in status:
        if os.path.isfile(os.path.join(calc_dir, "best.mint")):
            return "Complete"
        else: return "Failed"
    else: return "Running"

def get_solution(name, number):
    '''
    Get the solution from a certain FPASS iteration

    :param name Identifying name of FPASS problem
    :param number Iteration number
    :return [Path to structure], [energy / atom]
    '''

    if not has_been_created(name):
        raise Exception("Problem has not initialized: " + name)
    calc_dir = get_calculation_directory(name,number)
    if not os.path.isdir(calc_dir):
        raise Exception("Calculation has not beed started: %s:%s"%(name,str(number)))
    status = get_solution_status(name, number)
    if not "Complete" in status:
        raise Exception("Calculation has not completed. Status: " + status)

    # Read in energy from mint output
    energy = -1
    try:
        fp = open(os.path.join(calc_dir, "stdout"), 'r')
        for line in fp:
            if "Energy per atom:" in line:
                energy = float(line.split()[3]); 
        fp.close()
    except:
        return None, None
    
    return os.path.join(calc_dir, "best.mint"), energy
