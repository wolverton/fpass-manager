'''
This module is designed to manage the execution of many First-Principles Assisted Structure Solution (FPASS)
 calculations. Key features of this package are:

    - Creating a directory to store FPASS calculations and results
    - Be able to determine status of an FPASS solution
    - Handle the validation of any proposed solution

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''

