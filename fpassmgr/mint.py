'''
Wrapper for commands performed by mint

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''
from tempfile import NamedTemporaryFile

from fpassmgr.settings import Settings
from qmpy import Calculation, Structure, io
import os
import subprocess
import tempfile

class Mint:

    @staticmethod
    def get_space_group_number(name):
        '''
        Given the name of a space group. return the number
        :param name: Name of spacegroup (any format)
        :return: Spacegroup number if matched, None if not
        '''

        call = "%s -space %s"%(Settings.mint, name)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

        line = proc.stdout.readline()
        if not "Space group information" in line:
            return None
        line = proc.stdout.readline()
        return int(line.split()[2])

    @staticmethod
    def compare(strc1, strc2, tol=0.01):
        '''
        Compare two structures.
        :param strc1 Path to file describing structure 1
        :param strc2 Path to file describing structure 2
        :param tol Optional: Tolerance parameter
        :return Whether the two are equal
        '''

        if not os.path.isfile(strc1):
            raise Exception('File does not exist: ' + strc1)
        if not os.path.isfile(strc2):
            raise Exception('File does not exist: ' + strc2)
        call = "%s %s %s -compare -tol %f"%(Settings.mint, strc1, strc2, tol)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

        output = ""
        for line in proc.stdout:
            if "are the same" in line: return True
            elif "are not the same" in line: return False
            output += line + "\n"
        raise Exception('Something failed with using mint to compare: ' + output)

    @staticmethod
    def convert(strc, to_format, filename=None):
        '''
        Convert a structure file from one format to another

        :param strc Path to file to be converted
        :param to_format Desired format (i.e. "vasp", "cmtx", "cif")
        :param filename Optional: Path to output file
        :return Text of converted file in string format
        '''


        if not os.path.isfile(strc):
            raise Exception('File does not exist: ' + strc)
        call = "%s %s -disp 0 -print screen %s"%(Settings.mint, strc, to_format)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

        output = ""
        for line in proc.stdout:
            output += line
        
        if filename != None:
            fp = open(filename, 'w')
            print >>fp, output
            fp.close()

        return output
        
    @staticmethod
    def get_space_group(strc, input_type="file", tol=0.0001):
        '''
        Get the spacegroup of a structure using Mint
        
        :param strc Something describing a structure
        :param input_file Format of strc. Options: "file"->path to file, "string"->print to file first, "qmpy"->QMPY Structure object
        :param tol Spacegroup detection tolerance
        :return Space group number
        '''
        
        ## Get the path the structure as a file
        if input_type is "file":
            path = strc
            clean = False
        elif input_type is "string":
            temp = tempfile.NamedTemporaryFile()
            path = temp.name
            clean = True
            fp = open(path,'w')
            print >>fp, strc
            fp.close()
        elif input_type is "qmpy":
            temp = tempfile.NamedTemporaryFile()
            path = temp.name
            io.poscar.write(strc,filename = path)
            clean = True
        else:
            raise Exception('Format not recognized' + input_type)
        
        ## Get the spacegroup
        call = "%s %s -disp 0 -tol %f -space"%(Settings.mint, path, tol)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
        
        spg = -1
        for line in proc.stdout:
            if "ERROR" in line: break
            if "number" in line:
                spg = float(line.split()[2])
        if spg == -1:
            raise Exception('Space group determination failed for: ' + path)
        
        ## Cleanup
        return spg

    @staticmethod
    def structure_to_primitive(strc, tol=0.0001):
        '''
        Convert a QMPY structure object to primitive cell (Niggli reduced, actually)
        
        :param strc QMPY structure object
        :param tol Symmetry tolerance
        :return QMPY structure object representing the primitive cell
        '''
        
        if not isinstance(strc, Structure): 
            raise Exception("First arguement must be a Structure")
        
        ## Write structure to a poscar
        temp_in = tempfile.NamedTemporaryFile()
        io.poscar.write(strc, filename=temp_in.name)
        
        ## Convert it to primitive cell
        call = "%s %s -primitive -reduced -tol %f -disp 0 -print screen vasp"%(Settings.mint, temp_in.name, tol)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

                ## Write result to a POSCAR
        temp_out = tempfile.NamedTemporaryFile()
        fp = open(temp_out.name,'w')
        for line in proc.stdout:
            print >>fp, line.rstrip()
        fp.close()
        
        ## Read in that POSCAR and exit
        newstrc = io.poscar.read(temp_out.name)
        return newstrc
        
    @staticmethod
    def structure_to_conventional(strc, tol=0.0001):
        '''
        Convert a QMPY structure object to conventional cell
        
        :param strc QMPY structure object
        :param tol Symmetry tolerance
        :return QMPY structure object representing the conventional cell
        '''
        
        if not isinstance(strc, Structure): 
            raise Exception("First arguement must be a Structure")
        
        ## Write structure to a poscar
        temp_in = tempfile.NamedTemporaryFile()
        io.poscar.write(strc, filename=temp_in.name)
        
        ## Convert it to conventional cell
        call = "%s %s -conv -tol %f -disp 0 -print screen vasp"%(Settings.mint, temp_in.name, tol)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

        ## Write result to a POSCAR
        temp_out = tempfile.NamedTemporaryFile()
        fp = open(temp_out.name,'w')
        for line in proc.stdout:
            print >>fp, line.rstrip()
        fp.close()
        
        ## Read in that POSCAR and exit
        newstrc = io.poscar.read(temp_out.name)
        return newstrc

    @staticmethod
    def refine(strc, tol=0.01):
        '''
        Given a QMPY structure object, adjust it such that atomic 
        positions are exactly on their high-symmetry site

        :param strc: QMPY structure object to be refined
        :param tol: Tolerance 
        :return New QMPY structure object representing refined structure
        '''

        # # Write structure to a poscar
        temp_in = tempfile.NamedTemporaryFile()
        io.poscar.write(strc, filename=temp_in.name)

        ## Convert it to conventional cell
        call = "%s %s -refine -tol %f -disp 0 -print screen vasp" % (Settings.mint, temp_in.name, tol)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

        ## Write result to a POSCAR
        temp_out = tempfile.NamedTemporaryFile()
        fp = open(temp_out.name, 'w')
        for line in proc.stdout:
            print >> fp, line.rstrip()
        fp.close()

        ## Read in that POSCAR and exit
        newstrc = io.poscar.read(temp_out.name)
        return newstrc

    @staticmethod
    def get_xray_match(strc, xray, refine=False, rietveld=False, get_pattern=False):
        '''
        Calculate the R factor between a structure and x-ray pattern.
        :param strc: Either path to structure file, or QMPY Structure
        :param xray: Path to xray file
        :param refine: Whether to adjust atomic positions to better match pattern
        :param rietveld: Whether to match/refine against the entire pattern
        :param get_pattern: Whether to return the pattern
        :return: R factor, [if get_pattern] refined diffraction pattern
        '''

        ## Check input
        if not os.path.isfile(xray):
            raise Exception("No such file: " + xray)

        ## Get path to structure
        if isinstance(strc, Structure):
            temp = NamedTemporaryFile()
            path = temp.name
            io.poscar.write(strc, filename=path)
        else:
            path = strc

        ## Run the calculation
        if refine:
            if rietveld: commands = "-refine rietveld -diffraction cont"
            else: commands = "-refine -diffraction"
        else:
            if rietveld: commands = "-diffraction cont"
            else: commands = "-diffraction"
        call = "%s %s %s %s -disp 0 -print screen vasp" % (Settings.mint, path, xray, commands)
        proc = subprocess.Popen(call, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

        ## Import the result
        rFactor = "Failure"
        pattern_lines = []
        reading_pattern = False
        for line in proc.stdout:
            # Logic for reading in pattern
            if reading_pattern:
                if "----" in line: continue
                if len(line) < 2: reading_pattern = False # Done
                else: pattern_lines.append(line)
            if "Two-theta" in line: reading_pattern = get_pattern
            # Logic for getting the R factor
            if "R factor" in line:
                rFactor = float(line.split()[2])

        ## Return results
        if not get_pattern:    return rFactor

        pattern = []
        for line in pattern_lines:
            words = line.split()
            pattern.append([float(words[0]), float(words[1])])
        return rFactor, pattern
