'''
Handle structure solution by a database searching method.

This includes both selecting all compounds that fit a certain stoichiometry
and using a machine learning technique to select which ones to evaluate

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''

from tempfile import NamedTemporaryFile
from qmpy import Structure, Formation, io
from qmpy.analysis.symmetry.spacegroup import Spacegroup
from fpassmgr.manager import * 
from fpassmgr.mint import *
from fpassmgr.prototypes import *

'''
Holds static methods that handle finding potential prototypes for a certain structure.

Current methods:
    1. Get all structures with same stoichiometry and spacegroup which have been reported as stable in OQMD (dH < 0)
'''
class DSCSP:

	@staticmethod
	def gather_matching_prototypes(name, spacegroup_match = "number", start_calcs=False):
		'''
		Lay the groundwork for a prototype-based structure solution. Does the following:

			1. Finds all prototypes that match the description of a certain FPASS problem.
			2. Write out structures of all names
			 	a. If desired, start their calculations now
			3. Save matching prototypes to a library

		:param name: Name of FPASS problem
		:param spacegroup_match: What about the spacegroup needs to match (i.e. "nothing", "number", or "system")
		:param start_calcs: Whether to actually start calculations, or just write out the files in preparation for them TBD
		'''

		if not has_been_created(name):
			raise Exception("FPASS problem not started: " + name)
		p_dir = get_prototype_directory(name)
		if os.path.isdir(p_dir):
			raise Exception("Prototype search already started:" + name)

		## Get all matching prototypes
		matching = DSCSP._get_matching_prototypes(name, spacegroup_match=spacegroup_match)

		## Write them out
		comp = get_example_solution(name).composition.formula
		os.mkdir(p_dir)
		for prototype in matching.keys():
			DSCSP.write_prototype(comp, p_dir, prototype)

		## Save the library
		lib_dir = os.path.join(p_dir, 'library')
		PrototypeLibraryFactory.save_to_directory(matching, lib_dir)

	@staticmethod
	def candidate_prototypes(name):
		'''
		Get list of candidate prototypes being considered for a particular problem
		:param name: Name of FPASS problem
		:return: List of prototype names, list of paths to their structures
		'''

		p_dir = get_prototype_directory(name)

		if not os.path.isdir(p_dir):
			if not has_been_created(name):
				raise Exception("No such FPASS problem")
			else: return [], []

		names = []; paths = []
		for d in os.listdir(p_dir):
			if d == "library": continue
			path = os.path.join(p_dir, d)
			if os.path.isdir(path):
				names.append(d)
				paths.append(os.path.join(path,"strc.vasp"))

		return names, paths

	@staticmethod
	def write_prototype(comp, directory, prototype):
		'''
		Writes out directories in which to run prototype validation for any symmetrically-distinct instances a
		prototype structure at a certain composition

		If there is more than one symmetrically-distinct image, will write out them with the format
		:param comp: Composition of structure being solved
		:param directory: Directory in which to write candidates
		:param prototype: PrototypeStructure to be evaluated
		'''

		instances = prototype.create_instances(comp)

		if len(instances) == 1:
			path = os.path.join(directory, prototype.name)
			if not os.path.isdir(path):
				os.mkdir(path)
				path = os.path.join(path, "strc.vasp")
				io.poscar.write(instances[0], path)
		else:
			for i in range(len(instances)):
				path = os.path.join(directory, prototype.name + "_" + str(i))
				if os.path.isdir(path): continue
				os.mkdir(path)
				path = os.path.join(path, "strc.vasp")
				io.poscar.write(instances[i], path)

	@staticmethod
	def _lookup_prototypes(generic, spacegroup_match, spg_num = None):
		'''
		Find all candidate prototypes given formula
		:param generic: Generic formula (e.g. A2B7)
		:param spacegroup_match: What about the spacegroup needs to match (i.e. "nothing" or "number" or "system")
		:param spg_num: Optional - Number of target space group
		:return: PrototypeStructure library
		'''

		## Lookup space group
		if spg_num is None:
			spg = None
		else:
			spg = Spacegroup.get(spg_num)

		## Get compounds
		form = Formation.objects.filter(composition__generic=generic, delta_e__lte=0)
		form = form.exclude(calculation__path__contains='prototype')
		if spacegroup_match == "number":
			if spg is None:
				raise Exception("Space group needs to be specified")
			form = form.filter(calculation__input__spacegroup__number=spg.number)
		elif spacegroup_match == "system":
			if spg is None:
				raise Exception("Space group needs to be specified")
			form = form.filter(calculation__input__spacegroup__lattice_system=spg.lattice_system)
		elif spacegroup_match != "nothing":
			raise Exception("Space group match not recognized: " + spacegroup_match)

		## Get the structures
		ids = form.values_list('calculation__input__id', flat=True)
		strcs = Structure.objects.filter(id__in=ids)

		## Get the prototypes
		library = PrototypeLibraryFactory.get_unique_prototypes(strcs)
		return library

	@staticmethod
	def _get_matching_prototypes(name, spacegroup_match = "number"):
		'''
		Find all candidate prototypes for a certain structure. Does so by searching OQMD for all suitable prototypes

		:param name: Name of FPASS problem
		:param spacegroup_match: What about the space group needs to match (i.e. "nothing" or "number" or "system")
		:return: PrototypeStructure library
		'''

		## Get the stoichiometry
		obj = get_example_solution(name)
		generic = obj.composition.generic

		## Get the spacegroup, formula
		spg_num = get_target_spacegroup(name)

		## Filter out prototypes
		library = DSCSP._lookup_prototypes(generic, spacegroup_match, spg_num)
		return library
