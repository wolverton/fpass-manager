'''
Operations allowing one to interface with a supercomputer queue

Author(s):
    Logan Ward (ward.logan.t@gmail.com)
'''

from fpassmgr.settings import *
import os
import subprocess
import re
import tempfile
import shutil

_job_statuses = None

def qsub(script):
    '''
    Call "qsub" to submit a job to the queue. Writes the job ID
     to a file named "run_id" in the current directory
    
    :param qsub Path to the batch script
    :return Job ID
    '''
    global _job_statuses
    call = Settings.qsub + " " + script +  " > temp"
    if not Settings.debug:
        subprocess.call(call, shell=True)

        # Read in the job ID
        fp = open('temp','r')
        temp = fp.readline()
        fp.close()
        m = re.search("^[0-9]*",temp)
        if m == None: 
            raise Exception("Job submission failed.")
        try:
            run_id = int(m.group(0))
        except:
            print "Failed to parse run id %s in dir: %s"%(m.group(0), os.getcwd())
            raise
        os.remove("temp")
        
        # Write it to file
        fp = open('run_id', 'w')
        print >>fp, run_id
        fp.close()
    
        # Mark status as running
        if _job_statuses is None:
            _job_statuses = {}
        _job_statuses[int(run_id)] = "Q"
    
        return run_id
    else:
        fp = open("run_id",'w')
        print >>fp, "1"
        fp.close()
        return -1

def qsub_in_dir(script, directory):
    '''
    Change directory, call "qsub <script>", and change back
    
    :param script Path to job script (relative to <directory>)
    :param directory Directory in which to submit job
    '''
    
    cwd = os.getcwd()
    os.chdir(directory)
    qsub(script)
    os.chdir(cwd)

def get_calculation_status(path):
    '''
    Get the status of a calculation, given path

    Job must have been started with the above qsub operation (creating a run_id) file

    :param path Path to the calculation directory
    :return Status (see get_job_status)
    '''

    run_file = os.path.join(path, "run_id")
    if not os.path.isfile(run_file):
            raise Exception('Run ID file not found for ' + path)
    fp = open(run_file, 'r')
    try:
        line = fp.readline()
        if len(line) == 0:
            raise Exception()
        run_id = int(line)
    except:
        raise Exception("Failed to read status for path: %s"%path)
    fp.close()

    return get_job_status(run_id)

def get_job_status(job_id):
    '''
    Get the status of a job. Can return:
        Queued - If it is waiting to be run
        Running - If job is currently running on a node
        Complete - If no record of job is found
    :param job_id ID number of job
    :return Status (as string)
    '''
    global _job_statuses
    if _job_statuses is None:
        _job_statuses = get_all_job_statuses()

    # Parse the output
    if job_id in _job_statuses.keys():
        status = _job_statuses[job_id]
        if status == "R": return "Running"
        elif status == "Q": return "Queued"
        else: return "Complete"
    else: return "Complete"

def get_all_job_statuses():
    '''
    Get the status of all jobs for the current user 

    :return Dictionary of job ID to status
    '''

    output = {}

    # Make the call
    call = Settings.qstat
    proc = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

    # Compile the regex
    nre = re.compile("^[0-9]*")

    # Get the statuses
    proc.stdout.readline() ## Header
    proc.stdout.readline() ## Header
    for line in proc.stdout:
        words = line.split()
        if len(words) < 2: continue
        num = int(nre.findall(words[0])[0])
        stat = words[4]
        output[num] = stat

    return output    

def edit_queue_file(path, name=None, proc=None):
    '''
    Edit some aspects of a queue file

    :param path Path to queue file to be editted
    :param name Optional: New name of job
    :param proc Optional: [Number of nodes, processors per node]
    '''
    
    if not os.path.isfile(path):
        raise Exception("File does not exist: " + path)
    if proc != None and len(proc) != 2:
        raise Exception("proc must have length == 2")

    ## Make a backup copy 
    temp = tempfile.NamedTemporaryFile()
    shutil.copy(path, temp.name)
    
    ## Make edits
    fi = open(temp.name, 'r')
    fo = open(path, 'w')
    for line in fi:
        if "#PBS -N" in line and name != None:
            line = "#PBS -N " + name[:14]
        if "#PBS -l nodes" in line and proc != None:
            line = "#PBS -l nodes=%d:ppn=%d"%(proc[0],proc[1])
        print >>fo, line.rstrip()
    fi.close(); fo.close()
