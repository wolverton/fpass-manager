'''
Operations necessary for generating the FPASS status website

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''

from numpy.core.function_base import linspace
from operator import getitem, itemgetter
from scipy.interpolate.interpolate import spline
from fpassmgr.util import xray_data_is_processed, get_xray_pattern
from fpassmgr.validation import FPASSValidation, Validation
from fpassmgr.manager import *
from fpassmgr.automate import *
import os
import shutil
import cgi
import time
from settings import Settings
from user import UI
from qmpy import INSTALL_PATH as qmpy_path
from matplotlib import pyplot as p
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

'''
Static class designed to hold website-making operations
'''
class WebsiteMaker:
    ''' Header for FPASS problem status tables '''
    fpass_header = '<tr><th>Name</th><th>Composition</th><th>Number Atoms</th><th>Space Group</th><th>Status</th></tr>'
        
    @staticmethod
    def make_website():
        '''
        Call this option to make the website
        '''

        ## Copy in necessary files
        shutil.copy(os.path.join(Settings.template_dir, "style.css"), Settings.website_dir)
        if not os.path.isdir(os.path.join(Settings.website_dir, 'jsmol')):
            shutil.copytree(os.path.join(qmpy_path,'web','static','js','jsmol'), 
                os.path.join(Settings.website_dir, 'jsmol'))
        fo = open(os.path.join(Settings.website_dir, "index.html"),'w')

        print >>fo, "<html>"

        ## Print head
        print >>fo, "<head>"
        print >>fo, "\t<title>FPASS Status</title>"
        print >>fo, '\t<link rel="stylesheet" href="style.css" type="text/css" media="screen" />'
        print >>fo, "</head>"

        print >>fo, "<body>"
        print >>fo, "<div id=\"wrapper\">"

        ## Print Header
        print >>fo, '\t<div id="header">'
        print >>fo, "\t\t<h1>FPASS Manager Status</h1>"
        print >>fo, '\t</div>'
        print >>fo, "<p>Status of FPASS problems as of %s"%time.strftime("%c")

        ## Get summary of currently-running jobs
        running = []; complete = [];
        for name in get_current_problems():
            row = WebsiteMaker._print_summary_row(name)
            if "Validated" in row: 
                complete.append(row)
            else: running.append(row)

        ## Print summary table for running porblems
        print >>fo, "\t<h2>Currently-Running Problems</h2>"
        print >>fo, "\t<table border='1'>"
        print >>fo, "\t\t" + WebsiteMaker.fpass_header
        for row in running:
            print >>fo, "\t\t" + row
        print >>fo, "\t</table>"

        ## Print out active alerts
        print >>fo, "\t<h2>Unresolved Errors</h2>"
        errors = Automation.check_for_errors()
        if len(errors) == 0:
            print >>fo, "\t<p>No active errors!</p>"
        else:
            print >>fo, "\t<table border='1'>"
            print >>fo, "\t\t<tr><th>Problem</th><th>Error Message</th></tr>"
            for problem, errors in errors.iteritems():
                for error in errors:
                    print >>fo, "\t\t<tr><td>%s</td><td>%s</td></tr>"%(problem, error)
            print >>fo, "\t</table>"

        ## Print out the recent automatic actions
        print >>fo, "\t<h2>Recent Actions</h2>"
        print >>fo, "\t<table border='1'>"
        print >>fo, "\t\t<tr><th>Date</th><th>Problem</th><th>Action</th></tr>"
        fi = open(Settings.action_log, 'r')
        actions = fi.read().split("\n")
        fi.close()
        for line in actions[-10:]:
            words = line.split("\t")
            if len(words) < 3: continue
            print >>fo, "\t\t<tr><td>%s</td></tr>"%("</td><td>".join(words))
        print >>fo, "\t</table>"

        ## Print summary table for running porblems
        print >>fo, "\t<h2>Solved Problems</h2>"
        print >>fo, "\t<table border='1'>"
        print >>fo, "\t\t" + WebsiteMaker.fpass_header
        for row in complete:
            print >>fo, "\t\t" + row
        print >>fo, "\t</table>"

        ## Close up shop
        print >>fo, "</div>"
        print >>fo, "</body>"
        print >>fo, "</html>"
        fo.close()

    @staticmethod
    def _print_summary_row(name):
        '''
        Print out the summary of an FPASS problem as a table row
        :param name: Name of FPASS problem
        :return: Row describing it
        '''

        output = "<tr>"
        output += "<td><a href=\"%s/index.html\">%s</a></td>"%(name,name) # Print name
        WebsiteMaker._make_problem_webpage(name)

        ## Get info about solution
        strc = get_example_solution(name)
        output += "<td>%s</td>"%(strc.composition.html)
        output += "<td>%d</td>"%(strc.natoms)
        output += "<td>%s</td>"%str(get_target_spacegroup(name))

        ## Status information
        output += "<td>%s</td>"%(UI.short_status(name))

        output += "</tr>"
        return output

    @staticmethod
    def _make_problem_webpage(name):
        '''
        Make a webpage describing status of a single FPASS problem
        :param name: Name of FPASS problem
        '''

        if not has_been_created(name):
            raise Exception('No such FPASS problem: ' + name)

        path = os.path.join(Settings.website_dir, name)
        if os.path.isdir(path):
            shutil.rmtree(path)
        os.mkdir(path)

        ### Write webpage about this problem
        fo = open(os.path.join(path,'index.html'),'w')
        print >>fo, "<html>"

        ## Write header
        print >>fo, "<head>"
        print >>fo, "\t<title>Status: %s</title>"%(name)
        print >>fo, '\t<link rel="stylesheet" href="../style.css" type="text/css" media="screen" />'
        print >>fo, '\t<script type="text/javascript" src="../jsmol/JSmol.min.js"></script>'
        print >>fo, "</head>"

        ## Write Body
        print >>fo, "<body>"
        print >>fo, "<div id=\"wrapper\">"

        # Header
        print >>fo, "<div id=\"header\">"
        print >>fo, "\t<h1>Status of %s</h1>"%name
        print >>fo, "</div>"

        # Write full status (content)
        print >>fo, "<div id=\"content\">"
        print >>fo, "\t<h2>Full Status</h2>"
        print >>fo, "\t<p>"
        for line in WebsiteMaker._text_to_html(UI.check_status(name)).split("\n"):
            print >>fo, "\t\t" + line
        print >>fo, "\t</p>"

        # Write validation status message (content)
        print >>fo, "\t<h2>Validation Results</h2>"
        print >>fo, "\t<h3>Summary</h3>"
        print >>fo, "\t<ul>"
        val_result, details = FPASSValidation.validate_solution(name, details=True)
        for line in WebsiteMaker._text_to_html(val_result, line_breaks=False).split("\n"):
            print >>fo, "\t\t<li>" + line + "</li>"
        print >>fo, "\t</ul>"

        # Write validation details (content)
        strc, agreement, eng = FPASSValidation.get_optimal_solution(name)
        if "\n" in val_result: # Has finished
            print >>fo, "\t<h3>Stability Result</h3>"
            calc_dir = os.path.join(FPASSValidation.get_validation_directory(name), 'calc')
            imgPath = os.path.join(path, 'stability.png')
            WebsiteMaker._write_stability_figure(imgPath, details['stability'])
            print >>fo, "\t<center><img src='stability.png'></center>"

            print >>fo, "\t<h3>Volume Change Result</h3>"
            imgPath = os.path.join(path, 'voldiff.png')
            WebsiteMaker._write_voldiff_figure(imgPath, details['volume'])    
            print >>fo, "\t<center><img src='voldiff.png'></center>"

            xray = os.path.join(get_problem_directory(name), "xray.in")
            if os.path.isfile(xray) and not xray_data_is_processed(xray):
                print >>fo, "\t<h3>X-Ray Diffraction Pattern Match</h3>"
                imgPath = os.path.join(path, 'xray.png')
                WebsiteMaker._write_diffraction_pattern(strc, xray, imgPath, calc_pattern=details['r'])
                imgPath = os.path.join(path, 'xray.svg')
                WebsiteMaker._write_diffraction_pattern(strc, xray, imgPath, calc_pattern=details['r'], size=[1000,500])
                print >>fo, "\t<center><a href=\"xray.svg\"><img src='xray.png'></a></center>"

            if os.path.isfile(xray):
                print >>fo, "\t<h3>Pareto Plot</h3>"
                imgPath = os.path.join(path, 'pareto.png')
                WebsiteMaker._write_pareto_plot(details['pareto'], imgPath)
                print >>fo, "\t<center><img src='pareto.png'></center>"

        print >>fo, "</div>"

        # Print structure (sidebar)
        print >>fo, '<div id="sidebar">'
        print >>fo, "<h2>Structure</h2>"
        if strc is None:
            print >>fo, "\t<p>No solution yet</p>"
        else:
            slnPath = os.path.join(path, name)
            Mint.convert(strc, 'cif', filename=slnPath + '.cif')
            Mint.convert(strc, 'vasp', filename=slnPath + '.vasp')
            temp = io.poscar.read(slnPath + '.vasp')
            io.cif.write(temp, slnPath + '.cif.plot')

            print >>fo, "\t<p>Solution has been found: <a href=\"%s.cif\">cif</a> <a href=\"%s.vasp\">vasp</a>"%(name,name)
            print >>fo, "\t<script>"
            print >>fo, """
                var Info = {
                    j2sPath: "..//jsmol/j2s",
        
                    src: "%s.cif.plot",
        
                    use: "HTML5",
                    width: 400,
                    height: 300,
                    readyFunction: null,
                    debug: false,
                    disableJ2SLoadMonitor: true,
                    disableInitialConsole: true
                  };
                  jma = Jmol.getApplet("myJmol", Info);
            """%name
            print >>fo, "\t</script>"
        print >>fo, "</div>"

        # Done writing body
        print >>fo, "</div>"
        print >>fo, "</body>"

        ## We're done with the page
        print >>fo, "</html>"
        fo.close()

    @staticmethod
    def _text_to_html(text, line_breaks=True):
        '''
        Escapes HTML special characters, Adds HTML linebreaks to text.
        :param text: Text to be formatted
        :return: Formatted text
        '''

        temp = cgi.escape(text)
        output = ""
        started = False
        for line in temp.split("\n"):
            if started:
                if line_breaks:    output += "\n</br>"
                else: output += "\n"
            output += line
            started = True
        return output

    @staticmethod
    def _write_voldiff_figure(path, vdiff, size=[500,250], dpi=72):
        '''
        Write a figure comparing the volume change on relaxation to that from the ICSD
        :param path: Path to which to write image file
        :param vdiff: Volume change (fractional) of this problem
        :param size: Size of file (in pixels)
        :param dpi: DPI of image
        '''

        ## Load in ICSD histogram
        fp = open(os.path.join(Settings.template_dir, 'vol-change.hist'), 'r')
        fp.readline()
        center = []; density = [];
        for line in fp:
            if len(line) < 2: continue
            words = line.split()
            center.append(float(words[0]) * 100)
            density.append(float(words[1]))
        fp.close()

        ## Make the basic plot
        p.clf()
        x = linspace(min(center),max(center),300)
        y = spline(center, density, x)
        ax = p.gca()
        ax.set_xlim([min(center),max(center)])
        ax.set_ylim([0, max(y)*1.1])
        p.plot(x, y)

        ## Add in the annotation arrow
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()

        # Get arrow head / text location
        arrow_length = (xlim[1]-xlim[0]) * 0.15
        vdiff_percent = vdiff * 100
        if vdiff_percent < min(center):
            xy = [xlim[0], ylim[1] / 2]
            xytext = [xlim[0] + arrow_length, ylim[1] / 2]
        elif vdiff_percent > max(center):
            xy = [xlim[1], ylim[1] / 2]
            xytext = [xlim[1] - arrow_length, ylim[1] / 2]
        else:
            xy = [vdiff_percent, 0]
            xytext = [vdiff_percent, (ylim[1]-ylim[0]) * 0.25]

        # Make the arrow
        ax.annotate("This\nStructure", xy=xy, xytext=xytext, size=16,
                    arrowprops=dict(facecolor='red', ec='red'))

        ## Make plot pretty
        ax.set_xlabel('Volume Difference (%)', fontsize=16)
        ax.set_ylabel('Fraction ICSD', fontsize=16)

        ## Print it out
        figsize = []
        for s in size: figsize.append(float(s) / dpi)
        p.gcf().set_size_inches(figsize)
        p.savefig(path, dpi=dpi, transparent=True)

    @staticmethod
    def _write_stability_figure(path, stability, size=[500,250], dpi=72):
        '''
        Write a figure comparing the stability to that from the ICSD
        :param path: Path to which to write image file
        :param stability: Stability of this structure (meV/atom)
        :param size: Size of file (in pixels)
        :param dpi: DPI of image
        '''

        ## Load in ICSD histogram
        fp = open(os.path.join(Settings.template_dir, 'stability.hist'), 'r')
        center = []; density = [];
        fp.readline()
        for line in fp:
            if len(line) < 2: continue
            words = line.split()
            center.append(float(words[0]))
            density.append(float(words[1]) * 100)
        fp.close()

        ## Make the basic plot
        p.clf()
        x = linspace(min(center),max(center),300)
        y = spline(center, density, x)
        ax = p.gca()
        ax.set_xlim([min(center),max(center)])
        ax.set_ylim([0, max(y)*1.1])
        p.plot(x, y)

        ## Add in the annotation arrow
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()

        # Get arrow head / text location
        arrow_length = (xlim[1]-xlim[0]) * 0.15
        if stability < min(center):
            xy = [xlim[0], ylim[1] / 2]
            xytext = [xlim[0] + arrow_length, ylim[1] / 2]
        elif stability > max(center):
            xy = [xlim[1], ylim[1] / 2]
            xytext = [xlim[1] - arrow_length, ylim[1] / 2]
        else:
            xy = [stability, 0]
            xytext = [stability, (ylim[1]-ylim[0]) * 0.25]

        # Make the arrow
        ax.annotate("This\nStructure", xy=xy, xytext=xytext, size=16,
                    arrowprops=dict(facecolor='red', ec='red'))

        ## Make plot pretty
        ax.set_xlabel('Stability (meV/atom)', fontsize=16)
        ax.set_ylabel('Fraction ICSD (%)', fontsize=16)

        ## Print it out
        figsize = []
        for s in size: figsize.append(float(s) / dpi)
        p.gcf().set_size_inches(figsize)
        p.savefig(path, dpi=dpi, transparent=True)

    @staticmethod
    def _write_diffraction_pattern(strc, xray, path, calc_pattern=None, size=[500,250], dpi=72):
        '''
        Generate a plot that shows the calculated and experimental diffraction calc_pattern.

        Only supports continuous diffraction patterns.
        :param strc: Path to structure
        :param xray: Path to xray file
        :param calc_pattern: Calculated calc_pattern (if already done)
        :param size: Size of image in pixels
        :param dpi: DPI of image
        :param path: Path to which to write file
        '''

        if xray_data_is_processed(xray):
            raise Exception("Processed patterns are not currently supported")

        exp_pattern = get_xray_pattern(xray)
        exp_pattern.sort(key=itemgetter(0))
        exp_angle, exp_inten = zip(*exp_pattern)

        if calc_pattern is None:
            r, calc_pattern = Mint.get_xray_match(strc, xray, refine=True, rietveld=True, get_pattern=True)
        calc_angle, calc_inten = zip(*calc_pattern)

        ## Make plot
        p.clf()
        p.plot(exp_angle, exp_inten, 'b-')
        p.plot(calc_angle, calc_inten, 'r--')

        ## Make it pretty
        ax = p.gca()
        ax.set_xlim([min(exp_angle), max(exp_angle)])
        ax.set_yticklabels([])
        ax.set_xlabel('Two Theta (degrees)', fontsize=16)
        ax.set_ylabel('Intensity', fontsize=16)

        ## Print it out
        figsize = []
        for s in size: figsize.append(float(s) / dpi)
        p.gcf().set_size_inches(figsize)
        p.savefig(path, dpi=dpi, transparent=True)

    @staticmethod
    def _write_pareto_plot(points, path, size=[500,250], dpi=72):
        '''
        Plot the x-ray match and energy of all tested structures

        :param points: Energy, R factor of each structure
        :param size: Size of image in pixels
        :param dpi: DPI of image
        :param path: Path to which to write file
        '''

        energies, matches = zip(*points)
        ## Find the lowest-energy structure
        lowest_eng = energies.index(min(energies))

        ## Find the best-matching structure
        best_match = matches.index(min(matches))

        ## Make plot
        p.clf()
        p.scatter(energies, matches, marker='.')

        ## Place the lowest energy, best match marker
        p.scatter(energies[lowest_eng], matches[lowest_eng], marker="<", s=60, c='r')
        p.scatter(energies[best_match], matches[best_match], marker="v", s=60, c='b')

        ## Decide on axis ranges
        ax = p.gca()
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()
        xmin = energies[lowest_eng] - 5
        xmax = min(energies[best_match] * 1.1, 500)
        xmax = max(xmax, 5)
        ymin = matches[best_match] * 0.9
        ymax = min(matches[lowest_eng] * 1.1, 1)
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])

        ## Add corresponding lines
        p.plot([energies[lowest_eng], energies[lowest_eng]], ax.get_ylim(), 'r--')
        p.plot(ax.get_xlim(), [matches[best_match], matches[best_match]], 'b--')

        ## Make it pretty
        ax.set_xlabel('Energy above min (meV/atom)', fontsize=16)
        ax.set_ylabel('R Factor', fontsize=16)

        ## Print it out
        figsize = []
        for s in size: figsize.append(float(s) / dpi)
        p.gcf().set_size_inches(figsize)
        p.savefig(path, dpi=dpi, transparent=True)
