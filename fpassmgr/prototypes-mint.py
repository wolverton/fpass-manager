'''
Handle everything that deals with protoype structures:

	(- Finding common prototypes amoung certain structures)
	(- Identifying which prototype a structure belongs to)
	(- Finding all possible prototypes at a certain composition)

Note: This version using Mint to compare
	
Authors:
	Logan Ward (ward.logan.t@gmail.com)
'''

from qmpy import Structure, Element
from mint import *
import itertools
import django.db as ddb
import tempfile
import os
import operator as op
import shutil

def prototypes_are_equal(strc1, strc2, local=True):
	'''
	Given two structures, determine whether they are based on the same prototype
	
	:param strc1 Either path to structure file, or QMPY structure object
	:param strc2 Either path to structure file, or QMPY structure object
	:param local If true, code assumes strc* are pathes to files. Otherwise, assumes they are VASP structure objects
	:return Whether the two are based on the same structure
	'''
	if local:
		a = PrototypeStructure.import_file(strc1)
		b = PrototypeStructure.import_file(strc2)
	else:
		a = PrototypeStructure.import_qmpy(strc1)
		b = PrototypeStructure.import_qmpy(strc2)
	return a.is_equivalent_to(b)
	
	
'''
Holds the structure of a certain prototype. Has the ability to:

	- Store a prototype structure
	- Test whether another prototype is equivalent to this one
	- 
'''
class PrototypeStructure:
	''' Structure of this prototype as QMPY Structure object '''
	_structure = None
	''' Name of this protoype, usually Composition-Spacegroup '''
	name = None
	''' Composition of this prototype (i.e. CsCl) '''
	comp = None
	''' Structural formula (i.e. AB) '''
	formula = None
	''' Original composition of structure '''
	_original_comp = None
	''' Original site identities '''
	_originalIdentities = []
	''' Original latice parameters '''
	_originalLatParam = None
	''' Spacegroup of structure '''
	spg = None

	def __str__(self):
		return self.name

	def __repr__(self): return self.name

	@staticmethod
	def import_file(path):
		'''
		Create a Prototype entry based off of a structure
		
		:param path Path to the structure file (any mint-readable format)
		:return Prototype describing this structure
		'''
		
		temp = tempfile.NamedTemporaryFile()
		Mint.convert(path, 'vasp', temp.name)
		obj = io.poscar.read(temp.name)
		return PrototypeStructure.import_qmpy(obj)
		
	@staticmethod	
	def import_qmpy(obj):
		'''
		Create a Prototype entry based off of a structure
		
		:param obj QMPY structure object
		:return Prototype describing this structure
		'''
		
		out = PrototypeStructure()
		## Step #1: Compute the spacegroup of this structure using Mint
		out.spg = Mint.get_space_group(obj, input_type='qmpy')
		
		## Step #2: Get a name for this prototype
		out.formula = obj.composition.generic
		out.comp = obj.composition.formula
		out.name = "%s-%d"%(out.comp, out.spg)
		
		## Step #3: Convert to conventional cell with standardized lattice parameters.
		## The goal is to avoid difficulties like much different a:c ratios or angles, which 
		##  makes it difficult to match candidates. (Ex: AlB2)
		## Solution: Pick standard angles and ratios, scale lattice constants depending on natoms
		out._structure = Mint.structure_to_conventional(obj, tol=0.01)
		out._originalLatticeParam = out._structure.cell
		if out.spg <= 2: #Triclinic
			out._structure.lat_params = [ 1, 2, 3, 80, 90, 110 ] # Triclinic
		elif out.spg <= 15: 
			out._structure.lat_params = [ 1, 2, 3, 90, 80, 90 ] # Monoclinic
		elif out.spg <= 74: 
			out._structure.lat_params = [ 1, 2, 3, 90, 90, 90 ] # Orthorhombic
		elif out.spg <= 142: 
			out._structure.lat_params = [ 1, 1, 2, 90, 90, 90 ] # Tetragonal
		elif out.spg <= 194: 
			out._structure.lat_params = [ 1, 1, 2, 90, 90, 120 ] # Hexagonal/Trigonal
		else: 
			out._structure.lat_params = [ 1, 1, 1, 90, 90, 90 ] # Cubic
		out._structure.set_volume(1.0 * out._structure.natoms) # Makes atoms at least 1-ish angstroms apart
		
		## Step #4: Convert such that the most-prevalent atom is H, second most is He, ...
		comp = sorted([ (a,b) for a,b in out._structure.comp.items() ], key=op.itemgetter(1))
		out._originalIdentities = []
		for x in out._structure.atoms:
			out._originalIdentities.append(x.element)
			for z in range(len(comp)):
				if x.element.symbol == comp[z][0]: 
					x.element = Element.get(z+1)
					break
		
		return out
		
	def is_equivalent_to(self,other):
		'''
		Check whether another prototype is equivalent to this one.
		
		:param other PrototypeStructure to be compared against
		:return Whether they are equivalent
		'''
		
		if not isinstance(other, PrototypeStructure):
			raise Exception('Must be a PrototypeStructure')
		
		if self.formula != other.formula:
			return False # Have to have the same stoichiometry
		if self.spg != other.spg: return False
	
			
		this_poscar = tempfile.NamedTemporaryFile()
		other_poscar = tempfile.NamedTemporaryFile()
		answer = False
		
		## Print out other prototype's structure
		io.poscar.write(other._structure, filename=other_poscar.name)
		
		## For each equivalent structure
		for this_equiv in PrototypeStructure.generate_equivalent_structures(self._structure):
			io.poscar.write(this_equiv, filename=this_poscar.name)
			for tol in [0.1, 0.2, 0.4]:		
				answer = Mint.compare(this_poscar.name,other_poscar.name,tol=tol)
				if answer: break
			if answer: break
		
		## We're done
		return answer
		
	def is_prototype_of(self,strc):
		'''
		Test whether a structure is an example of this prototype
		
		:param strc QMPY structure object
		:return Whether strc is an example of this prototype
		'''

		if self.formula != strc.composition.generic: 
			return False
		
		return self.is_equivalent_to(PrototypeStructure.import_qmpy(strc))
		
	def save(self, directory):
		'''
		Save prototype to disk. Filename will be directory + self.name + ".prototype"

		Will be in POSCAR format
		
		:param directory 
		'''
		## Convert lattice parameters
		toOutput = self._structure.copy()
		toOutput.cell = self._originalLatticeParam
		## Set site compositions back to original
		for x,e in zip(toOutput.atoms,self._originalIdentities):
			x.element = e
		## Save it
		filename = os.path.join(directory, self.name + ".prototype")
		io.poscar.write(toOutput, filename)
		return filename
		
	@staticmethod
	def generate_equivalent_structures(strc):
		'''
		Generate all permutations where atom types with an equal number of atoms are swapped. These
		structures are not necessarily symmetrically-equivalent, but are based on the same prototype
		
		:return List of all possible structures that are the same composition, prototype, 
			but not (necessarily) symmetrically-equivalent
		'''
		## Generate a list containing the types of atoms with certains by the number in the unit cell
		equivSites = []
		for element,count in strc.comp.items():
			found = False
			for number, list in equivSites:
				if number == count:
					found = True
					list.append(element)
			if not found:
				equivSites.append((count, [element]))
		equivSites = sorted(equivSites, key=op.itemgetter(0), reverse=True)
		siteList = [ y for x,y in equivSites ]
		
		## Explode it! 
		allOrders = PrototypeStructure.explode_combinations(siteList)
		
		## Generate all equivalent combinations
		originalOrder = allOrders[0]
		output = []
		for order in allOrders:
			newStructure = strc.copy()
			for atom in newStructure.atoms:
				id = originalOrder.index(atom.element.symbol)
				atom.element = Element.get(order[id])
			output.append(newStructure)
		return output
			
	@staticmethod
	def explode_combinations(equivList):
		'''
		Generate all equivalent combinations of elements in a list, which are provided
		as all equivalent items for the first positions, all items for the next
		set of positions, and so on. For example:

		Given [ [A,B], [C,D], E ]: Will return ABCDE, ABDCE, BACDE, BADCE
		:param equivList 2D list, each element is a of list of items
		:return List of all permutations of all in eL[0], then all permutations in eL[1]
		'''
		if len(equivList) == 0: return []
		elif len(equivList) == 1:
			if len(equivList[0])==1: return equivList
			else: return [ x for x in itertools.permutations(equivList[0]) ]
		else:
			endings = PrototypeStructure.explode_combinations(equivList[1:])
			headings = itertools.permutations(equivList[0])
			output = []
			for head in headings:
				for tail in endings:
					new = list(head)
					new.extend(list(tail))
					output.append(new)
			return output
			

'''
Holds static operations	that operate on a library of equivalent prototypes

A library is simply a map of PrototypeStructure ==> set([ compositions exhibiting this prototype ])
'''	
class PrototypeLibraryFactory:
	''' Whether to print status to file '''
	print_status = False

	@staticmethod
	def get_unique_prototypes(strc_list):
		'''
		Generate a list of unique prototypes out of a list of QMPY structure objects
		
		:param strc_list List of QMPY structure objects
		'''
		if PrototypeLibraryFactory.print_status:
			fp = open('status.log', 'w')
		library = {}
		for strc in strc_list:
			compound = strc.composition.formula
			try: 
				name = PrototypeLibraryFactory.add_structure(library, strc)
			except:
				if PrototypeLibraryFactory.print_status:
					print >>fp, 'Matching failed for %s'%(compound)
				continue
			if PrototypeLibraryFactory.print_status:
				print >>fp, 'Matched %s to %s'%(compound, name)
			ddb.reset_queries()
		if PrototypeLibraryFactory.print_status:
			fp.close()	
		return library
		
	@staticmethod
	def add_structure(library, strc):
		'''
		Add a structure to a prototype library.
		
		If a prototype is new, add it to the library. Otherwise, add structure as an example 
		for matching prototype
		
		:param library Library to be added to
		:param strc New structure
		:return name Name of new prototype
		'''

		for prototype, examples in library.iteritems():
			if prototype.is_prototype_of(strc):
				examples.add(strc.composition.formula)
				return prototype.name
		new = PrototypeStructure.import_qmpy(strc)
		library[new] = set([ strc.composition.formula ])
		return new.name

	@staticmethod 
	def add_prototype(library, toadd, ex=None):
		'''
		Add a new prototype to library.

		If prototype already exists, composition of prototype (and any examples of that prototype)
		will be added as examples to matching prototype

		:param library Library to be added to
		:param toadd Prototype to be added
		:param ex Optional: Set of examples of toadd
		'''

		for prototype, examples in library.iteritems():
			if prototype.is_equivalent_to(toadd):
				examples.add(toadd.comp)
				if not ex is None:
					examples.update(ex)
				return
		if not ex is None:
			ex = ex.add(toadd.comp)
		else:
			ex = set([toadd.comp])
		library[toadd] = ex

	@staticmethod
	def get_applicable_prototypes(library, formula):
		'''
		Given the structural formula, get all possible prototypes

		:param library Library from which to extract prototypes
		:param formula Structural formula (i.e. A4B9)
		:return Library that contains only prototypes with specified formula
		'''

		newLibrary = {}
		for prototype, examples in library.iteritems():
			if prototype._structure.composition.generic == formula:
				newLibrary[prototype] = examples
		return newLibrary
	
	@staticmethod
	def save_to_directory(library, path):
		'''
		Save a prototype library to disk

		:param library Library to be printed
		:param path Directory in which to store prototypes
		'''

		## Make the directory
		if os.path.isdir(path):
			shutil.rmtree(path)
		os.mkdir(path)	

		## Print out everything
		for strc, examples in library.iteritems():
			strc.save(path)
			fp = open(os.path.join(path, strc.name + '.examples'), 'w')
			for e in examples:
				print >>fp, e
			fp.close()

	@staticmethod
	def load_from_directory(path):
		'''
		Load a prototype library from disk

		:param path Directory in which library is stored
		:return Library stored in that directory
		'''
	
		if not os.path.isdir(path):
			raise Exception('No such directory: ' + path)

		library = {}
		for f in os.listdir(path):
			if not "prototype" in f: continue

			name = f.split(".")[0]
			p = PrototypeStructure.import_file(os.path.join(path,f))
			ex = set([])
			fp = open(os.path.join(path, name + ".examples"), 'r')
			for e in fp:
				ex.add(e.rstrip())
			library[p] = ex
			fp.close()
		return library
