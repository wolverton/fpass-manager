'''
Interface to entries involving the Powder-Diffraction File. Provides the following functionality:

    - Check whether a PDF entry is suitable for FPASS
    - Organize library of PDF entries
    - Get the next-easiest PDF entry to run
    - Start an FPASS problem given PDF entry

To-Do List:
    - Check whether it is possible to generate a random structure before starting

Authors:
    Logan Ward (ward.logan.t@gmail.com)

'''
from ase.lattice.spacegroup.spacegroup import Spacegroup

from fpassmgr.manager import *
import re
from math import modf
from qmpy.materials.entry import Entry
from qmpy.utils.strings import parse_comp

'''
Static class that holds commands to deal with PDF entries
'''
class PDFInterface:

    @staticmethod
    def pdf_entry_is_suitable(path, max_natoms = 50):
        '''
        Read in a PDF entry in cif format, determine whether it is suitable for FPASS. Checks whether there has to
        be partial-occupancy for a structure to be possible.

        NOTE: Really, what I want is how many in primitive cell. But, that requires reading spacegroups, which means I need
        to be able to handle non-standard spacegroups.

        :param path: Path to the entry
        :param max_natoms: Maximum number of atoms in unit cell cell that I would run
        :return: Whether it is suitable, why it is unsuitable, number of atoms
        '''

        ## Check if it exists
        if not os.path.isfile(path):
            raise Exception("No such file: " + path)

        ## Get the atom count
        fp = open(path, 'r')
        atoms = []
        z = 1
        hasPositions = False
        spg = None
        composition = None
        for line in fp:
            if "_chemical_formula_sum" in line:
                composition = " ".join(line.split()[1:]) # Get composition
                num_matcher = re.compile("[0-9\.]+")
                for elem in re.findall('[A-Z][a-z]?[0-9\.]*',composition):
                    count = num_matcher.findall(elem)
                    if len(count) == 0:
                        atoms.append(1.0)
                    else:
                        atoms.append(float(count[0]))
            if "space_group_name" in line:
                spg = "".join(line.split()[1:])
            if "_cell_formula_units_Z" in line:
                z = float(line.split()[1])
            if "_atom_site_fract_x" in line:
                hasPositions = True
        fp.close()
        if len(atoms) == 0:
            raise Exception("No composition found in pdf entry: " + path)
        natoms = sum(atoms) * z
        if hasPositions:
            return False, "Solution already known.", natoms

        ## Check if there is a non-integer amount of any atom
        for atom in atoms:
            amount = z * atom
            if modf(amount)[0] > 1e-6 and 1.0 - modf(amount)[0] > 1e-6:
                return False, "Must have partial occupancy", natoms

        ## Check whether structure is known in QMPY (via the ICSD)
        spg_num = Mint.get_space_group_number(spg)
        if spg_num is None:
            return False, "Error: Unknown space group", natoms
        comp = parse_comp(composition)
        formula = ' '.join(['%s%g' % (k, comp[k]) for k in sorted(comp.keys())])
        hits = Entry.objects.filter(path__contains = 'icsd', composition__formula = formula, structure__spacegroup__number = spg_num).count()
        if hits > 0:
            return False, "Structure already known to OQMD", natoms

        ## Check whether it is possible to generate a random structure
        try:
            Mint.get_space_group(path)
        except:
            return False, "ERROR: Could not generate example solution", natoms
        fp.close()

        ## Check number of atoms is small enough
        if natoms < max_natoms:
            return True, "Looks fine to me", natoms
        else:
            return False, "Too many atoms in unit cell", natoms

    @staticmethod
    def add_pdf_entry(path):
        '''
        Determines whether a PDF entry is suitable for FPASS, and copies it to an appropriate folder in the ICDD entry

        :param path: Path to PDF entry (in CIF format)
        :return: New path
        '''

        if not os.path.isdir(Settings.pdf_dir):
            raise Exception("PDF directory is missing: " + Settings.pdf_dir)

        ## Check applicability of this entry
        applicable, reason, natoms = PDFInterface.pdf_entry_is_suitable(path)

        ## Put it in the appropriate directory
        filename = os.path.basename(path)
        if applicable:
            dest = os.path.join(Settings.pdf_dir, "suitable")
        else:
            if "Too many atoms" in reason:
                dest = os.path.join(Settings.pdf_dir, "too-large")
            elif "known" in reason:
                if "OQMD" in reason:
                    dest = os.path.join(Settings.pdf_dir, "known-oqmd")
                else:
                    dest = os.path.join(Settings.pdf_dir, "known")
            elif "ERROR" in reason:
                dest = os.path.join(Settings.pdf_dir, "error")
            else:
                dest = os.path.join(Settings.pdf_dir, "unsuitable")
        if not os.path.isdir(dest):
            os.mkdir(dest)
        shutil.copy(path, os.path.join(dest,filename))
        return os.path.join(dest,filename)

    @staticmethod
    def add_all_pdf_entries(directory):
        '''
        Added all PDF entries contained in a directory
        :param directory: Directory containing PDF entries [cif format]
        :return Number added
        '''

        added = 0
        for file in os.listdir(directory):
            if not "cif" in file: continue
            PDFInterface.add_pdf_entry(os.path.join(directory,file))
            added += 1
        return added

    @staticmethod
    def get_unsolved_pdf_entries(number = 10):
        '''
        Get the top unsolved entries (by difficulty)

        For now, computes difficulty as number of atoms in the unit cell

        :param number: Number of easiest entries to present
        :return: Names of ICDD entries
        '''

        good_directory = os.path.join(Settings.pdf_dir, "ready")
        good_entries = [ x for x in os.listdir(good_directory) if "cif" in x ]
        entries = {}
        for entry in good_entries:
            path = os.path.join(good_directory, entry)
            ## Get difficulty
            fp = open(path, 'r')
            atoms = []
            z = 1
            for line in fp:
                if "_chemical_formula_sum" in line:
                    composition = " ".join(line.split()[1:]) # Get composition
                    num_matcher = re.compile("[0-9\.]+")
                    for elem in re.findall('[A-Z][a-z]?[0-9\.]*',composition):
                        count = num_matcher.findall(elem)
                        if len(count) == 0:
                            atoms.append(1.0)
                        else:
                            atoms.append(float(count[0]))
                if "_cell_formula_units_Z" in line:
                    z = float(line.split()[1])
            difficulty = sum(atoms) * z

            ## Store result
            name = re.sub('.cif', '', entry)
            entries[name] = difficulty

        ## Store them by difficulty
        return sorted(entries, key=entries.get)[:number]

    @staticmethod
    def start_entry(name):
        '''
        Start FPASS solution of a PDF entry
        :param path: Name of PDF entry
        :return Name of problem
        '''

        ## Get path to PDF entry of that name
        path = os.path.join(Settings.pdf_dir, 'ready', name + '.cif')
        if not os.path.isfile(path):
            raise Exception("No such PDF entry: " + name)

        ## Call it to be started
        PDFInterface._start_entry(name, path)

        ## Move it to the "started" directory
        started = os.path.join(Settings.pdf_dir, "started")
        if not os.path.isdir(started):
            os.mkdir(started)
        shutil.move(path, started)

        return name

    @staticmethod
    def extract_xray_pattern(path):
        '''
        Extracts xray pattern out of a PDF cif entry
        :param path: Path to file
        :return: Wavelength, xray pattern (lines of text)
        '''
        fp = open(path, 'r')
        wavelength = 1.54056
        while True:
            line = fp.readline()
            if not line:
                raise Exception("Pattern import failed.")
            if "_diffrn_radiation_wavelength" in line:
                wavelength = float(line.split()[1])
            if ("_pd_meas_counts_total" in line) or ("_pd_meas_intensity_total" in line): break
        xray_data = []
        line = fp.readline()
        while len(line) > 2:
            xray_data.append(" ".join(line.rstrip().split()[-2:]))
            line = fp.readline()
        fp.close()
        return wavelength, xray_data

    @staticmethod
    def _start_entry(name, path):
        '''
        Internal use only: Submits a PDF entry to the FPASS manager
        :param name: Desired name of FPASS problem
        :param path: Path to PDF entry (cif format)
        '''

        ## Write out the  structure input file
        # Convert the coordinates
        strc_file = NamedTemporaryFile()
        Mint.convert(path, 'mint free', filename=strc_file.name)

        ## Get the spacegroup number, write in strc file
        # Get spacegroup from cif file
        fp = open(path, 'r')
        spg = None
        for line in fp:
            if "_space_group_" in line and not "system" in line:
                spg = line.split()[1]
                break
        fp.close()
        if spg is None:
            raise Exception("No spacegroup in file: " + path)
        spg_num = Mint.get_space_group_number(spg)
        if spg_num is None:
            raise Exception("Mint does not recognize spacegroup: " + spg)

        # Read in the strc file
        fp = open(strc_file.name, 'r')
        lines = fp.readlines()
        fp.close()

        # Ensure lattice parameters do not get changed
        lines[1] = lines[1].rstrip() + " t t t"
        lines[2] = lines[2].rstrip() + " t t t"
        lines.append("space %d"%(spg_num))

        # Write it back out
        fp = open(strc_file.name, 'w')
        for line in lines:
            print >>fp, line.rstrip()
        fp.close()

        ## Write out XRD pattern file
        # Read XRD pattern from cif file
        wavelength, xray_data = PDFInterface.extract_xray_pattern(path)

        # Write it out
        xray_file = NamedTemporaryFile()
        print >>xray_file, "wavelength %.5f"%(wavelength)
        for point in xray_data:
            print >>xray_file, point
        xray_file.flush()

        ## Submit to manager
        initialize_problem_directory(name, strc_file.name, xray=xray_file.name)

        return name
