'''
Holds operations used for automatically managing the currently-running
FPASS problems. 

Key features:

    - Decide when to start new FPASS solutions
    - Start validation of proposed solutions
    - Bring manager's attention to failures

How to use:

    Continue running FPASS problems: Automation.automate()
    Find unresolved (i.e. not automatically-fixable) errors: Automation.check_for_errors()

Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''
from time import strftime, localtime
from fpassmgr.pdf import PDFInterface
from fpassmgr.validation import FPASSValidation
from fpassmgr.manager import *
from fpassmgr.torque import qsub_in_dir

import os

'''
Holds automation-related static methods. 
'''
class Automation:
    ''' Maximum number of solutions to start at once '''
    _max_start = 5
    ''' Maximum total solutions for a single problem count '''
    _max_total_solutions = 30
    ''' Maximum number of problems to have automatically running '''
    _max_problems = 3

    @staticmethod
    def automate(restart_all_failed = False):
        '''
        Continues currently-running FPASS problems. Will
            - Check for failed jobs, maybe fix them
            - Start new FPASS calcualtions, if necessary
            - Start validation, if minimum agreement has been reached
        :param restart_all_failed: Whether to result all failed jobs, regardless of reason
        '''

        num_problems_in_progress = 0
        num_problems_complete = 0

        for problem in get_current_problems():
            num_problems_in_progress += 1

            # Get status of each FPASS solution
            statuses = get_solution_statuses(problem)

            ## Handle any failed jobs
            failed_jobs = [ k for k,v in statuses.iteritems() if v == "Failed"]
            if len(failed_jobs) > 0:
                for job in failed_jobs:
                    path = get_calculation_directory(problem, job)
                    reason = Automation.diagnose_fpass_failure(path)
                    solution = Automation.handle_fpass_failure(path, reason)
                    if solution != False:
                        action = "Handled \"%s\" in %s."%(reason, job)
                        Automation._update_log(problem, action)
                    elif restart_all_failed:
                        qsub_in_dir("../sub.q", path)
                        action = "Restarted job: %s"%(job)
                        Automation._update_log(problem, action)
                continue

            ## Start new FPASS solutions if needed
            # Don't start new jobs if there is a failure
            if len(failed_jobs) > 0: continue

            # Don't start new jobs if at least one job has not succeed
            num_complete = len([k for k,v in statuses.iteritems() if v == "Complete"])
            if num_complete < 1: continue

            num_running = len([k for k,v in statuses.iteritems() if v == "Running"])


            # Start enough jobs for quorum to be reached
            if num_complete + num_running < FPASSValidation.min_quorum:
                to_start = min(Automation._max_start, FPASSValidation.min_quorum - num_complete)
                if len(statuses) + to_start < Automation._max_total_solutions:
                    start_fpass_solutions(problem, to_start)
                    Automation._update_log(problem, "Not enough solutions to reach quorum. Started %d"%to_start)
                continue

            # If their are enough solutions started to reach quorum, check if agreement has been reached
            strc, agreement, energy = FPASSValidation.get_optimal_solution(problem)
            if agreement < FPASSValidation.min_agreement:
                # If the previous jobs have not finished
                if num_running > 0: continue
                # If not, start some new jobs
                to_start = (FPASSValidation.min_agreement - agreement) / \
                    (float(agreement) / float(num_complete)) # [ # Needed ] / [ P(finding best) ]
                to_start -= num_running
                if to_start <= 0: continue
                to_start = min(Automation._max_start, to_start)
                if len(statuses) + to_start < Automation._max_total_solutions:
                    start_fpass_solutions(problem, to_start)
                    Automation._update_log(problem, "Not enough solutions in agreement. Started %d"%to_start)
            else:
                if num_running > 0: continue
                val_status = FPASSValidation.get_status(problem)
                if "Not started" in val_status: 
                    FPASSValidation.start_validation(problem)
                    Automation._update_log(problem, "Agreement has been reached. Started validation")
                elif "Complete" in val_status:
                    num_problems_in_progress -= 1

        ## If enough problems are not running, start more
        if num_problems_in_progress < Automation._max_problems:
            to_start = Automation._max_problems - num_problems_in_progress
            pdf_entries = PDFInterface.get_unsolved_pdf_entries(to_start)
            for entry in pdf_entries:
                name = PDFInterface.start_entry(entry)
                start_fpass_solutions(name, 1)
                Automation._update_log(name, "Started new problem")

    @staticmethod
    def _update_log(problem, message):
        '''
        Add automated action to the log
        :param problem: Name of problem
        :param message: Message to record
        '''

        if not os.path.isfile(Settings.action_log):
            raise Exception('Action log not found!')

        fp = open(Settings.action_log, 'a')
        print >>fp, "\t".join([strftime("%Y-%m-%d %H:%M:%S",localtime()), problem, message])
        fp.close()

    @staticmethod
    def check_for_errors():
        '''
        Checks all currently-running FPASS problems for errors that require user intervention.

        TO DO: Add in check for validation failure
    
        :return Library that maps problem name to list of strings that describe unresolved errors
        '''
        errors = {}
        for problem in get_current_problems():
            e = Automation._check_for_errors(problem)
            if len(e) > 0:
                errors[problem] = e
        return errors


    @staticmethod
    def _check_for_errors(problem):
        '''
        Checks a single FPASS problem for errors.

        Current checks:
            - Whether individual calculations have failed

        :return List of errors
        '''

        if not has_been_created(problem):
            return ['Not yet created']

        errors = []
        ## Get calculation statuses
        slns = get_solution_statuses(problem)    
        for sln, stat in slns.iteritems():
            if stat == "Failed":
                reason = Automation.diagnose_fpass_failure(get_calculation_directory(problem, sln))
                errors.append('Solution %s failed: %s'%(str(sln), reason))
        if len(errors) > 0:
            return errors

        ## Check if validation has failed
        val_status = FPASSValidation.validate_solution(problem)
        if "FAILED" in val_status:
            return ['Failure during validation'] 
        return []

    @staticmethod
    def diagnose_fpass_failure(path):
        '''
        Given path to a failed FPASS calculation, diagnose what is wrong

        :param path Path to FPASS caluclation directory
        :return Reason why job failed
        '''

        if not os.path.isdir(path):
            return "Directory does not exist"

        ## Check the stdout file for clues
        stdout = os.path.join(path, 'stdout')
        if not os.path.isfile(stdout):
            return "Mint was never called"

        ## Check for errors in OPT_STR_1 log
        opt_log = os.path.join(path,'OPT_STR_1','OPT_1','log.out')
        if not os.path.isfile(opt_log):
            return "Optimization never started"
        fp = open(opt_log, 'r')
        for line in fp:
            if "ERROR" in line:
                fp.close()
                if "VASP potential was not supplied" in line:
                    return "Missing VASP psuedopotential"
                elif "not commensurate with space group" in line or \
                    "do not fit space group symmetry" in line:
                    return "Bad structure during optimization"
                elif "No diffracted intensities were set" in line:
                    return "XRD pattern import failure"
                else:
                    return "Unknown error in OPT log"
        fp.close()

        return "Unknown error"

    @staticmethod
    def handle_fpass_failure(path, reason=None):
        '''
        Given path to a FAILED FPASS solution, handle it appropriately

        :param path: Path to FPASS calculation directory
        :param reason: Reason for FPASS failure
        :return False if nothing could be done. Action otherwise
        '''

        if not os.path.isdir(path):
            raise Expection("No such path: " + path)

        if reason is None:
            reason = Automation.diagnose_fpass_failure(path)

        ## Error handling
        if "Bad structure during optimization" in reason:
            # Just restart it
            qsub_in_dir(os.path.join("..", "sub.q"), path)
            return "Restarted job"
        elif "XRD pattern import failure" in reason:
            # Add "gaoptuserietveld true" to settings
            setPath = os.path.join(path, "..", "set.in")
            fp = open(setPath, 'a')
            print >>fp, "gaoptuserietveld true"
            fp.close()
            # Restart it
            qsub_in_dir(os.path.join("..", "sub.q"), path)
            return "Switched to Rietveld, Restarted job"
        else:
            return False
