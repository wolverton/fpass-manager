'''
Handle everything that deals with prototype structures:

    - Finding common prototypes among a list of structures:
        How to: PrototypeLibraryFactory.get_unique_prototypes
    - Identifying which prototype a structure belongs to
        How to: PrototypeLibraryFactory.find_patching_prototype
    - Finding all possible prototypes at a certain stoichiometry (given library)
        How to: PrototypeLibraryFactor.get_applicable_prototypes

    
Authors:
    Logan Ward (ward.logan.t@gmail.com)
'''

import operator
import copy
from qmpy import Structure, Element
from qmpy.analysis.symmetry import *
from mint import *
import itertools
import django.db as ddb
import tempfile
import os
import sys
import operator as op
import shutil

def prototypes_are_equal(strc1, strc2, local=True):
    '''
    Given two structures, determine whether they are based on the same prototype
    
    :param strc1 Either path to structure file, or QMPY structure object
    :param strc2 Either path to structure file, or QMPY structure object
    :param local If true, code assumes strc* are pathes to files. Otherwise, assumes they are VASP structure objects
    :return Whether the two are based on the same structure
    '''
    if local:
        a = PrototypeStructure.import_file(strc1)
        b = PrototypeStructure.import_file(strc2)
    else:
        a = PrototypeStructure.import_qmpy(strc1)
        b = PrototypeStructure.import_qmpy(strc2)
    return a.is_equivalent_to(b)
    
    
'''
Holds the structure of a certain prototype. Has the ability to:

    - Store a prototype structure
    - Test whether another prototype is equivalent to this one
    - 
'''
class PrototypeStructure:
    ''' Structure of this prototype as QMPY Structure object '''
    _structure = None
    ''' Name of this protoype, usually Composition-Spacegroup '''
    name = None
    ''' Composition of this prototype (i.e. CsCl) '''
    comp = None
    ''' Structural formula (i.e. AB) '''
    formula = None
    ''' Original composition of structure '''
    _original_comp = None
    ''' Original site identities '''
    _originalIdentities = []
    ''' Original latice parameters '''
    _originalLatParam = None
    ''' Spacegroup of structure '''
    spg = None

    def __str__(self):
        return self.name

    def __repr__(self): return self.name

    @staticmethod
    def import_file(path):
        '''
        Create a Prototype entry based off of a structure
        
        :param path Path to the structure file (any mint-readable format)
        :return Prototype describing this structure
        '''
        
        temp = tempfile.NamedTemporaryFile()
        Mint.convert(path, 'vasp', temp.name)
        obj = io.poscar.read(temp.name)
        return PrototypeStructure.import_qmpy(obj)
        
    @staticmethod    
    def import_qmpy(obj):
        '''
        Create a Prototype entry based off of a structure
        
        :param obj QMPY structure object
        :return Prototype describing this structure
        '''
        
        out = PrototypeStructure()

        ## Refine positions to line up exactly onto high-symmetry sites
        obj = Mint.refine(obj, tol=0.01)

        ## Compute the spacegroup of this structure
        if obj.spacegroup is None:
            obj.symmetrize(tol = 0.01)
        out.spg = obj.spacegroup.number;
        
        ## Get a name for this prototype
        out.formula = obj.composition.generic
        out.comp = "".join(obj.composition.formula.split(" "))
        out.name = "%s-%d"%(out.comp, out.spg)
        
        ## Convert to conventional cell 
        ##  The goal is to avoid difficulties like much different a:c ratios or angles, which 
        ##  makes it difficult to match candidates. (Ex: AlB2)
        ## Solution: When checking, adjust the lattice parameters of other prototype to match this one's
        out._structure = Mint.structure_to_conventional(obj, tol=0.01)
        out._originalLatticeParam = out._structure.cell
        
        ## Convert such that the most-prevalent atom is H, second most is He, ...
        comp = sorted([ (a,b) for a,b in out._structure.comp.items() ], key=op.itemgetter(1))
        out._originalIdentities = []
        for x in out._structure.atoms:
            out._originalIdentities.append(x.element)
            for z in range(len(comp)):
                if x.element.symbol == comp[z][0]: 
                    x.element = Element.get(z+1)
                    break
        
        return out
        
    def is_equivalent_to(self,other, max_tol=0.3):
        '''
        Check whether another prototype is equivalent to this one. Method:

        1. Generate all symmetrically-distinct examples of a prototype (e.g., AB and BA if AB!=BA)
        2. For gradually increasing tolerances:
            a. Generate the primitive cells of each structure
            b. Adjust them so that their lattice parameters are the same
            c. Check whether they are symmetrically-equivalent
                i. If so, the two prototypes are identical
        
        :param other PrototypeStructure to be compared against
        :return Whether they are equivalent
        '''
        
        if not isinstance(other, PrototypeStructure):
            raise Exception('Must be a PrototypeStructure')
        
        if self.formula != other.formula:
            return False # Have to have the same stoichiometry
            
        answer = False

        ## For each equivalent structure
        equivs = PrototypeStructure.generate_equivalent_structures(self._structure)
        for tol in [max_tol / 4, max_tol / 2, max_tol]:
            for this_equiv in equivs:
                other_prim, t = other._structure.reduce(in_place = False, tol=tol)
                other_prim.symmetrize(tol = tol)
                this_prim, t = this_equiv.reduce(in_place = False, tol=tol)
                this_prim.symmetrize(tol = tol)
                other_prim.cell = this_prim.cell
                try:
                    answer = other_prim.compare(this_prim, tol=tol)
                except:
                    continue
                if answer: break
            if answer: break
        
        ## We're done
        return answer
        
    def is_prototype_of(self,strc):
        '''
        Test whether a structure is an example of this prototype
        
        :param strc QMPY structure object
        :return Whether strc is an example of this prototype
        '''

        if self.formula != strc.composition.generic: 
            return False
        
        return self.is_equivalent_to(PrototypeStructure.import_qmpy(strc))
        
    def save(self, directory):
        '''
        Save prototype to disk. Filename will be directory + self.name + ".prototype"

        Will be in POSCAR format
        
        :param directory 
        '''
        ## Convert lattice parameters
        toOutput = self.get_original_structure()
        ## Save it
        filename = os.path.join(directory, self.name + ".prototype")
        io.poscar.write(toOutput, filename)
        return filename

    def get_original_structure(self):
        '''
        Get a QMPY Structure representing original input structure
        :return: Original structure
        '''
        toOutput = self._structure.copy()
        toOutput.cell = self._originalLatticeParam
        ## Set site compositions back to original
        for x,e in zip(toOutput.atoms,self._originalIdentities):
            x.element = e
        return toOutput

    def create_instances(self, composition):
        '''
        Generate an instance of this prototype with a certain composition
        :param composition: String describing the desired composition
        :return: A list of all symmetrically-distinct structures with desired composition based on this prototype
        '''

        comp = parse_comp(composition)
        if format_generic_comp(comp) != self.formula:
            raise Exception("Composition does not match this formula of %s: %s,%s:"%(self.name, format_generic_comp(comp),self.formula))

        ## Generate original structure
        original = self.get_original_structure()

        ## Match up atoms
        oldcomp = sorted(original.comp, key=original.comp.get)
        newcomp = sorted(comp, key=comp.get)
        new = copy.copy(original)
        toReplace = {}
        for o,n in zip(oldcomp, newcomp):
            toReplace[o] = n
        new.substitute(toReplace, in_place=True,rescale_method='absolute')

        ## Generate all possible equivalents
        new_list = PrototypeStructure.generate_equivalent_structures(new)

        ## Find the unique ones
        output = []
        for x in new_list:
            found = False
            for y in output:
                if x.compare(y):
                    found = True
                    break
            if not found: output.append(x)
        return output


    @staticmethod
    def generate_equivalent_structures(strc):
        '''
        Generate all permutations where atom types with an equal number of atoms are swapped. These
        structures are not necessarily symmetrically-equivalent, but are based on the same prototype
        
        :return List of all possible structures that are the same composition, prototype, 
            but not (necessarily) symmetrically-equivalent
        '''
        ## Generate a list containing the types of atoms with certains by the number in the unit cell
        equivSites = []
        for element,count in strc.comp.items():
            found = False
            for number, list in equivSites:
                if number == count:
                    found = True
                    list.append(element)
            if not found:
                equivSites.append((count, [element]))
        equivSites = sorted(equivSites, key=op.itemgetter(0), reverse=True)
        siteList = [ y for x,y in equivSites ]
        
        ## Explode it! 
        allOrders = PrototypeStructure.explode_combinations(siteList)
        
        ## Generate all equivalent combinations
        originalOrder = allOrders[0]
        output = []
        for order in allOrders:
            newStructure = strc.copy()
            for atom in newStructure.atoms:
                id = originalOrder.index(atom.element.symbol)
                atom.element = Element.get(order[id])
            output.append(newStructure)
        return output
            
    @staticmethod
    def explode_combinations(equivList):
        '''
        Generate all equivalent combinations of elements in a list, which are provided
        as all equivalent items for the first positions, all items for the next
        set of positions, and so on. For example:

        Given [ [A,B], [C,D], E ]: Will return ABCDE, ABDCE, BACDE, BADCE
        :param equivList 2D list, each element is a of list of items
        :return List of all permutations of all in eL[0], then all permutations in eL[1]
        '''
        if len(equivList) == 0: return []
        elif len(equivList) == 1:
            if len(equivList[0])==1: return equivList
            else: return [ x for x in itertools.permutations(equivList[0]) ]
        else:
            endings = PrototypeStructure.explode_combinations(equivList[1:])
            headings = itertools.permutations(equivList[0])
            output = []
            for head in headings:
                for tail in endings:
                    new = list(head)
                    new.extend(list(tail))
                    output.append(new)
            return output
            

'''
Holds static operations    that operate on a library of equivalent prototypes

A library is simply a map of PrototypeStructure ==> set([ compositions exhibiting this prototype ])
'''    
class PrototypeLibraryFactory:
    ''' Whether to print status to file '''
    print_status = False

    @staticmethod
    def get_unique_prototypes(strc_list):
        '''
        Generate a list of unique prototypes out of a list of QMPY structure objects
        
        :param strc_list List of QMPY structure objects
        '''
        if PrototypeLibraryFactory.print_status:
            fp = open('status.log', 'w')
            count = 0

        ## Add each compound to library containing compounds with the same stoichiometry
        all_libraries = {}
        for strc in strc_list:
            # Get the library specific to this stoichiometry
            st = strc.composition.generic
            if not st in all_libraries:
                library = {}
                all_libraries[st] = library
            else:     
                library = all_libraries[st]

            # Add the compound to the appropriate library
            compound = strc.composition.formula
            try: 
                name = PrototypeLibraryFactory.add_structure(library, strc)
            except:
                if PrototypeLibraryFactory.print_status:
                    print >>fp, 'Matching failed for %s'%(compound)
                    fp.flush()
                continue
            if PrototypeLibraryFactory.print_status:
                print >>fp, 'Matched %s to %s'%(compound, name)
                count += 1
                if count % 10 == 0: fp.flush()
            ddb.reset_queries()
        if PrototypeLibraryFactory.print_status:
            fp.close()    
        
        ## Compile them to a single library    
        library = {}
        for lib in all_libraries.values():
            for k,v in lib.iteritems():
                library[k] = v
        return library
        
    @staticmethod
    def add_structure(library, strc):
        '''
        Add a structure to a prototype library.
        
        If a prototype is new, add it to the library. Otherwise, add structure as an example 
        for matching prototype
        
        :param library Library to be added to
        :param strc New structure
        :return name Name of new prototype
        '''

        for prototype, examples in library.iteritems():
            strc_proto = PrototypeStructure.import_qmpy(strc)
            if prototype.is_equivalent_to(strc_proto):
                examples.add("".join(strc.composition.formula.split()))
                return prototype.name
        new = PrototypeStructure.import_qmpy(strc)
        library[new] = set([ "".join(strc.composition.formula.split()) ])
        return new.name

    @staticmethod
    def find_matching_prototype(library, strc):
        '''
        Find which prototype (if any) matches a structure
        :param library: Library of prototype structures
        :param strc: QMPY Structure object, or path to a Mint-friendly structure file
        :return: Matching PrototypeStructure or None, examples of this structure
        '''

        ## Get the prototype of this structure
        if isinstance(strc, Structure):
            proto = PrototypeStructure.import_qmpy(strc)
        else:
            proto = PrototypeStructure.import_file(strc)

        ## See if anything in library matches
        for p, e in library.iteritems():
            if proto.is_equivalent_to(p):
                return p, e
        return None, None

    @staticmethod 
    def add_prototype(library, toadd, ex=None):
        '''
        Add a new prototype to library.

        If prototype already exists, composition of prototype (and any examples of that prototype)
        will be added as examples to matching prototype

        :param library Library to be added to
        :param toadd Prototype to be added
        :param ex Optional: Set of examples of toadd
        '''

        for prototype, examples in library.iteritems():
            if prototype.is_equivalent_to(toadd):
                examples.add(toadd.comp)
                if not ex is None:
                    examples.update(ex)
                return
        if not ex is None:
            ex = ex.add("".join(toadd.comp.split()))
        else:
            ex = set(["".join(toadd.comp.split())])
        library[toadd] = ex

    @staticmethod
    def get_applicable_prototypes(library, formula):
        '''
        Given the structural formula, get all possible prototypes

        :param library Library from which to extract prototypes
        :param formula Structural formula (i.e. A4B9)
        :return Library that contains only prototypes with specified formula
        '''

        newLibrary = {}
        for prototype, examples in library.iteritems():
            if prototype._structure.composition.generic == formula:
                newLibrary[prototype] = examples
        return newLibrary
    
    @staticmethod
    def save_to_directory(library, path):
        '''
        Save a prototype library to disk

        :param library Library to be printed
        :param path Directory in which to store prototypes
        '''

        ## Make the directory
        if os.path.isdir(path):
            shutil.rmtree(path)
        os.mkdir(path)    

        ## Print out everything
        for strc, examples in library.iteritems():
            strc.save(path)
            fp = open(os.path.join(path, strc.name + '.examples'), 'w')
            for e in examples:
                print >>fp, e
            fp.close()

    @staticmethod
    def load_from_directory(path):
        '''
        Load a prototype library from disk

        :param path Directory in which library is stored
        :return Library stored in that directory
        '''
    
        if not os.path.isdir(path):
            raise Exception('No such directory: ' + path)

        library = {}
        for f in os.listdir(path):
            if not "prototype" in f: continue

            name = f.split(".")[0]
            try:
                p = PrototypeStructure.import_file(os.path.join(path,f))
            except:
                print "Import Failure: %s"%f
                continue
            ex = set([])
            fp = open(os.path.join(path, name + ".examples"), 'r')
            for e in fp:
                ex.add(e.rstrip())
            library[p] = ex
            fp.close()
        return library

    @staticmethod
    def print_library(library, filename=None):
        '''
        Print the prototype of all examples in the library
        :param library: Library to be printed
        :param filename: File in which to print results (None if to screen)
        '''

        if filename is None:
            fp = sys.stdout
        else:
            fp = open(filename, 'w')

        for p, examples in library.iteritems():
            for e in examples:
                print >>fp, "%s %s"%(e, p.name)

        if not filename is None:
            fp.close()
