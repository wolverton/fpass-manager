'''
Holds settings for a specific system

Authors:
	Logan Ward (ward.logan.t@gmail.com)
'''

import os

'''
Just a static class for holding global settings
'''
class Settings:
	''' Path to FPASS calculations '''
	work_dir = os.environ['HOME'] + '/work'
	''' Path to directory containing template files and other useful things '''
	template_dir = os.environ['HOME'] + '/software/fpassmgr/files'
	''' Path to directory holding Powder Diffraction File entries '''
	pdf_dir = os.environ['HOME'] + "/icdd-files/"
	''' Path to directory housing FPASS webpage '''
	website_dir = os.environ['HOME'] + "/fpass-webpage/"
	''' Path to file containing recent actions '''
	action_log = os.environ['HOME'] + "/action.log"
	''' Whether to pretending to submit jobs to queue '''
	debug = False
	''' Path to qsub executable '''
	qsub = '/usr/local/bin/qsub'
	''' Path to qstat executable '''
	qstat = '/usr/local/bin/qstat'
	''' Path to mint executable '''
	mint = os.environ['HOME'] + '/bin/mint'
