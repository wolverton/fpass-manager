# FPASS Manager

FPASS Manager (fpassmgr) is a set of tools that automate using First-Principles-Assisted Structure Solution (FPASS) method to solve crystal structures.
Effectively, fpassmgr is a tool for managing 

## How do I get set up? 

You can install this software by cloning the repository and then calling `pip install -e .`. 

To set up your system to run FPASS calculations, you will also need to...

- Download and compile [Mint](https://github.com/materials/mint/)
- Adjust the settings in `settings.py` to point to the appropriate paths for each execuatable on your system
- Install a copy of [OQMD](oqmd.org), and make sure `qmpy` can properly query it (see instructions [here](http://oqmd.org/static/docs/getting_started.html))
- Edit the job submission scripts in `files` to work for your system
- Ensure that `bin` is on your path

## How do I use fpassmgr

Your primary way to interface with fpassmgr will be through the command line utility `fpassmgr`. 

For example, starting a new calculation can be accomplished by first calling `fpassmgr initialize [structure file] [XRD pattern file]`, which will provide the name of your FPASS run. 
Then, calling `fpassmgr start [name] 1` will start a new FPASS calculation.
One you have calculations running, calling `python -c "from fpassmgr.automate import Automation; Automation.automate()` will continue the FPASS solution workflow.

## Who do I talk to? 

If you have questions, please contact the developers by opening issues on this repository.